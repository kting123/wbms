<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TaboTabo extends Model {

	//
    public $timestamps = false;
   /* public function orderlist(){
        return $this->hasOne('\App\Cow_Article_Order');
    }*/
    public function article_resources(){
        return $this->hasMany('\App\Article_Resource');
    }

    public function expenses() {
        return $this->hasMany('App\Expense');
    }
    protected $fillable = ['status','customer'];
}
