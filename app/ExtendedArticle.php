<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ExtendedArticle extends Model
{

    //
    public function orderlists()
    {
        return $this->hasMany('App\Orderlist');
    }
}
