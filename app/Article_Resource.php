<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Article_Resource extends Model {

	//
    public function tabo_tabo(){
        return $this->belongsTo('\App\Tabo_Tabo');
    }
    public function cow(){
        return $this->belongsTo('\App\Cow');
    }
    public function article(){
        return $this->belongsTo('\App\Article');
    }

    public function orderlist(){
        return $this->hasMany('\App\Orderlist');
    }

    public function outsource(){
        return $this->belongsTo('\App\Outsource');
    }
    protected $fillable = ['tabo_tabo_id','article_id','sales_qty', 'unit_price', 'remain_qty','total_sales'];
}
