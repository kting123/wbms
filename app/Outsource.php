<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Outsource extends Model {

    public $timestamps = false;
    //
    public function article_resources() {
        return $this->hasMany('\App\Article_Resource');
    }

    public function payment_history(){
        return $this->hasMany('\App\Payment_History');
    }


}
