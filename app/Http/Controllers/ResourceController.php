<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ResourceController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Home Controller
      |--------------------------------------------------------------------------

      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function ViewResourcesTabo() {
        $pagination = \App\TaboTabo::where('tsk', '=', '0')->orderBy('id', 'desc')->paginate(8);
        $pagination->setPath('http://localhost/wbmsv1.0/public/view_tabo_tabo');
        return view('Admin.resources.tabotabo')
                        ->with('tabotabos', $pagination);
    }

    public function ViewFromOutsource() {
        $pagination = \App\Outsource::where('tsk', '=', '0')->orderBy('id', 'desc')->paginate(8);
        $pagination->setPath('http://localhost/wbmsv1.0/public/outsource');
        return view('Admin.resources.outsource')
                        ->with('outsource', $pagination);
    }

    public function ViewFromOwnFarm() {
        $pagination = \App\Cow::where('tsk', '=', '0')->orderBy('id', 'desc')->paginate(8);
        $pagination->setPath('http://localhost/wbmsv1.0/public/own');
        return view('Admin.resources.own')
                        ->with('own', $pagination);
    }

//    new--- Cindy
    public function ViewParts($id) {
        $cow = \App\Cow::find($id);
        $resource = \App\Article_Resource::where('cow_id', $id)->orderBy('id', 'desc')->get();
        $resource_choice_cuts = \App\Article_Resource::where('cow_id', $id)->where('type', 'Choice Cuts')->orderBy('id', 'desc')->paginate(5);
        $resource_choice_cuts->setPath('http://localhost/wbms_final/public/viewParts' . $id);
        $resource_special_cuts = \App\Article_Resource::where('cow_id', $id)->where('type', 'Special Cuts')->orderBy('id', 'desc')->paginate(5);
        $resource_special_cuts->setPath('http://localhost/wbms_final/public/viewParts' . $id);
        $resource_other_cuts = \App\Article_Resource::where('cow_id', $id)->where('type', 'Other Cuts')->orderBy('id', 'desc')->paginate(5);
        $resource_other_cuts->setPath('http://localhost/wbms_final/public/viewParts' . $id);
        $articles = \App\Article::all();
        $mainparts = \App\MainParts::where('cow_id', $id)->orderBy('id', 'desc')->paginate(5);
        $mainparts->setPath('http://localhost/wbms_final/public/viewParts' . $id);

        return view('subuser.cow_parts_cuts')->with('lists', $resource)
                        ->with('choice_cuts', $resource_choice_cuts)
                        ->with('special_cuts', $resource_special_cuts)
                        ->with('other_cuts', $resource_other_cuts)
                        ->with('articles', $articles)
                        ->with('cow', $cow)
                        ->with('mainparts', $mainparts);
    }

    public function DeleteParts($id) {
        $in = \Input::all();
//        dd($id);
        $mainparts = \App\MainParts::where('article_id', $id)
                ->where('cow_id', $in['cow_id'])
                ->first();
//        dd($mainparts);
        $mainparts->delete();
        \Flash::success('Successfully deleted!');
        return \Redirect::back();
    }

    public function DeleteCuts($id) {
        $in = \Input::all();
//        dd($id);        
        $article = \App\Article_Resource::where('article_id', $id)
                ->where('cow_id', $in['cow_id'])
                ->first();
//        dd($article);
        $article->delete();
        \Flash::success('Successfully deleted!');
        return \Redirect::back();
    }

    public function EditPart($id) {
        $in = \Input::all();
//        dd($in);
        $new_article_id = $this->getArticleId($in['part']);

        if ($id == $new_article_id && $in['old_qty'] == $in['qty']) {
//            dd($in);
            flash('You have no changes.');
            return \Redirect::back();
        } else {
            if ($id == $new_article_id) {
                $mainparts = \App\MainParts::where('article_id', $id)
                        ->where('cow_id', $in['cow_id'])
                        ->first();
                $mainparts->quantity = $in['qty'];
                $mainparts->save();
                \Flash::success('Successfully Updated!.');
                return \Redirect::back();
            } else {
                //check if new part already exist
                $check = \App\MainParts::where('cow_id', $in['cow_id'])
                                ->where('article_id', $new_article_id)->get();
                $var = json_decode(json_encode($check), TRUE);
                if ($var == NULL) {
                    //  dd($var);
                    $mainparts = \App\MainParts::where('article_id', $id)
                            ->where('cow_id', $in['cow_id'])
                            ->first();
                    $mainparts->article = $in['part'];
                    $mainparts->article_id = $new_article_id;
                    $mainparts->quantity = $in['qty'];
//            dd($mainparts);
                    $mainparts->save();

                    \Flash::success('Successfully Updated!.');
                    return \Redirect::back();
                } else {
                    \Flash::warning('Selected article already exist.');
                    return \Redirect::back();
                }
            }
        }
    }

    public function EditCut($id) {
        $in = \Input::all();
//        dd($in);
        $new_article_id = $this->getArticleId($in['article']);
//dd($new_article_id);
        if ($id == $new_article_id && $in['old_qty'] == $in['qty'] && $in['old_type'] == $in['type']) {
//            dd($in);
            flash('You have no changes.');
            return \Redirect::back();
        } else {
            if ($id == $new_article_id) {
                $article_resource = \App\Article_Resource::where('article_id', $id)
                        ->where('cow_id', $in['cow_id'])
                        ->first();
                $article_resource->quantity = $in['qty'];
                $article_resource->type = $in['type'];
                $article_resource->remain_qty = $in['qty'];
                $article_resource->save();
                flash('Successfully Updated!.');
                return \Redirect::back();
            } else {
                //check if new part already exist
                $check = \App\Article_Resource::where('cow_id', $in['cow_id'])
                                ->where('article_id', $new_article_id)->get();
                $var = json_decode(json_encode($check), TRUE);
                if ($var == NULL) {
                    //  dd($var);
                    $article_resource = \App\Article_Resource::where('article_id', $id)
                            ->where('cow_id', $in['cow_id'])
                            ->first();
                    $article_resource->article_id = $new_article_id;
                    $article_resource->quantity = $in['qty'];
                    $article_resource->remain_qty = $in['qty'];
                    $article_resource->type = $in['type'];
//            dd($article_resource);
                    $article_resource->save();

                    flash('Successfully Updated!.');
                    return \Redirect::back();
                } else {
                    flash('Selected article already exist.');
                    return \Redirect::back();
                }
            }
        }
    }

    public function AddTaboArticles($id) {
        $in = \Input::all();
//        dd($in);
        $count = count($in);
//        dd($count);
        $k = ($count - 2) / 2;
//        dd($k);
        for ($j = 0; $j < $k; $j++) {
//            dd($i);
            if ($in['article' . $id . $j] == '' || $in['qty' . $id . $j] == '') {
//                dd($i);
            } else {
                $article_id = $this->getArticleId($in['article' . $in['tabo_id'] . $j]);
                $check = \App\Article_Resource::where('tabo_tabo_id', $id)
                        ->where('article_id', $article_id)
                        ->get();
                $result = json_decode(json_encode($check), TRUE);
                if ($result == NULL) {
                    $article_id = $this->getArticleId($in['article' . $id . $j]);
                    $resource = new \App\Article_Resource;
                    $resource->tabo_tabo_id = $in['tabo_id'];
                    $resource->article_id = $article_id;
                    $resource->quantity = $in['qty' . $id . $j];
                    $resource->remain_qty = $in['qty' . $id . $j];
                    $resource->save();
                } else {
                    $resource = \App\Article_Resource::find($result[0]['id']);
                    $resource->remain_qty = $in['qty' . $id . $j];
                    $resource->quantity = $in['qty' . $id . $j];
                    $resource->save();
                }
            }
        }
        \Flash::success('Successfully Added!');
        return \Redirect::back();
    }

    public function ViewTaboArticles($id) {
//        dd($id);
        $tabo_tabo = \App\TaboTabo::find($id);
//        dd($tabo_tabo);
        $resource = \App\Article_Resource::where('tabo_tabo_id', $id)
                        ->orderBy('id', 'desc')->paginate(10);
        $resource->setPath('http://localhost/wbms_final/public/viewTaboArticles' . $id);
//        dd($resource);
        $articles = \App\Article::all();

        return view('subuser.Tabotabo_articles')->with('resource', $resource)
                        ->with('tabo_tabo', $tabo_tabo)
                        ->with('articles', $articles);
    }

    public function DeleteTaboArticle($id) {
        $in = \Input::all();
//        dd($in);
        $resource = \App\Article_Resource::where('article_id', $id)
                ->where('tabo_tabo_id', $in['tabo_tabo_id'])
                ->first();
//        dd($mainparts);
        $resource->delete();
        \Flash::success('Successfully deleted!');
        return \Redirect::back();
    }

    public function EditTaboArticle($id) {
        $in = \Input::all();
//        dd($in);
        $new_article_id = $this->getArticleId($in['article']);
//        dd($new_article_id);

        if ($id == $new_article_id && $in['old_qty'] == $in['qty']) {
//          dd($new_article_id);  
            flash('You have no changes.');
            return \Redirect::back();
        } else {
            if ($id == $new_article_id) {
                $article_resource = \App\Article_Resource::where('article_id', $id)
                        ->where('tabo_tabo_id', $in['tabo_tabo_id'])
                        ->first();
                $article_resource->quantity = $in['qty'];
                $article_resource->remain_qty = $in['qty'];
                $article_resource->save();
//                dd($article_resource);
                \Flash::success('Successfully Updated!.');
                return \Redirect::back();
            } else {
                //check if new article already exist
                $check = \App\Article_Resource::where('tabo_tabo_id', $in['tabo_tabo_id'])
                                ->where('article_id', $new_article_id)->get();
                $var = json_decode(json_encode($check), TRUE);
                if ($var == NULL) {
                    //  dd($var);
                    $article_resource = \App\Article_Resource::where('article_id', $id)
                            ->where('tabo_tabo_id', $in['tabo_tabo_id'])
                            ->first();
                    $article_resource->article_id = $new_article_id;
                    $article_resource->quantity = $in['qty'];
                    $article_resource->remain_qty = $in['qty'];
                    $article_resource->save();

                    \Flash::success('Successfully Updated!.');
                    return \Redirect::back();
                } else {
                    \Flash::warning('Selected article already exist.');
                    return \Redirect::back();
                }
            }
        }
    }

    public function SaveBaka($id) {
        $input = \Input::all();
        //  dd($input);
        // dd($input['month']);
        $cow = \App\Cow::find($id);
        $cow->id = $input['id'];
        $cow->sex = $input['sex'];
        $cow->color = $input['color'];
        $cow->date = $input['date_added'];
        flash('Successfully edited!');
        $cow->save();
        return \Redirect::back();
    }

    public function ResourceBreakdown($id) {
        $parts = \App\Article_Resource::where('cow_id', '=', $id)->get();
        dd($parts);
        foreach ($parts as $part) {
            $part2[] = $part->toArray();
        }
        //  dd($part2);
        return view('Admin.admin_resources_breakdown')->with('parts', $part2);
    }

    public function DeleteOutsource($id) {
        $data = \Input::all();
        $order = \App\Outsource::find($id);
        // \App\Orderlist::where('order_id', '=', $id)->delete();
        $order->delete();
        flash('Successfully deleted!');
        return \Redirect::back();
    }

    public function EditOutsource($id) {
        $data = \Input::all();
        //  dd($data);
        $order = \App\Outsource::find($id);
        $order->date = $data['date'];
        $order->dr = $data['dr'];
        $order->or = $data['or'];
        $order->ordered_from = $data['ordered_from'];
        $order->amount = $data['amount'];
        $order->status = $data['status'];
        $order->due_date = $data['due_date'];
        $order->save();
        flash('Successfully edited!');
        return \Redirect::back();
    }

    public function DeleteTabo($id) {
        $order = \App\TaboTabo::find($id);
        // dd($order);
        foreach ($order->article_resources as $article_recsource) {
            $article_recsource->article->remaining_quantity = $article_recsource->article->remaining_quantity - 1;
            $article_recsource->article->total_quantity = $article_recsource->article->total_quantity - 1;
            $article_recsource->article->save();
            $article_recsource->delete();
        }
        foreach ($order->expenses as $expense) {
            $expense->delete();
        }
        $order->delete();
        flash('Successfully deleted And Updated The Articles!');
        return \Redirect::back();
    }

    public function EditTabo($id) {
        $input = \Input::all();
        // dd($input);
        $tabo = \App\TaboTabo::find($id);
        $year = explode("/", $input['dateofpurchased']);
        $month = $this->GetMonth($year[0]);
        $tabo->date_of_purchase = $input['dateofpurchased'];
        $tabo->year = $year[2];
        $tabo->month = $month;
        $tabo->sex = $input['gender'];
        $tabo->lw = $input['lw'];
        $tabo->sw = $input['sw'];
        $tabo->mw = $input['mw'];
        $tabo->id = $input['bodyNum'];
        $tabo->price = $input['perKilo'] * $input['lw'];
        $tabo->price_per_kilo = $input['perKilo'];
        $tabo->owner = $input['owner'];
        $tabo->area = $input['area'];
        $tabo->color = $input['color'];
        $income_diff = $tabo->gross - $tabo->expense - $tabo->price;
        $tabo->income_deff = $income_diff;
        $tabo->save();
        flash('Item successfully updated!');
        return \Redirect::back();
    }

    public function DeleteOwn($id) {
        $cows = \App\Cow::find($id);
        foreach ($cows->article_resources as $cow) {
            $cow->article->remaining_quantity = $cow->article->remaining_quantity - $cow->quantity;
            $cow->article->total_quantity = $cow->article->total_quantity - $cow->quantity;
            $cow->article->save();
            $cow->delete();
        }
        $cows->delete();
        flash('Successfully deleted And Updated The Articles!');
        return \Redirect::back();
    }

    public function EditOwn($id) {
        $input = \Input::all();
        //  dd($input);
        $cow = \App\Cow::find($id);
        $cow->id = $input['bodyNum'];
        $cow->sex = $input['gender'];
        $cow->color = $input['color'];
        $cow->lw = $input['lw'];
        $cow->sw = $input['sw'];
        $cow->date = $input['dateofpurchased'];
        flash('Item successfully updated!');
        $cow->save();
        return \Redirect::back();
    }

    public function AddTabo() {
        //Tabo - Tabo
        $input = \Input::all();
        // dd($input);
        $tabo = new \App\TaboTabo;
        $year = explode("/", $input['dateofpurchased']);
        $month = $this->GetMonth($year[0]);
        $tabo->date_of_purchase = $input['dateofpurchased'];
        $tabo->year = $year[2];
        $tabo->month = $month;
        $tabo->sex = $input['gender'];
        $tabo->lw = $input['lw'];
        $tabo->id = $input['bodyNumber'];
        $tabo->sw = $input['sw'];
        $tabo->mw = $input['mw'];
        $tabo->price = $input['perKilo'] * $input['lw'];
        $tabo->price_per_kilo = $input['perKilo'];
        $tabo->expense = 0;
        $tabo->owner = $input['owner'];
        $tabo->area = $input['area'];
        $tabo->color = $input['color'];
        $income_diff = -1 * ($input['perKilo'] * $input['lw']);
        $tabo->gross = 0;
        $tabo->income_deff = $income_diff;
        $tabo->save();
        flash('Successfully Added');
        return \Redirect::back();
        //echo "success";
        //dd($tabo);
        //	dd($input);
    }

    public function AddDeliveryTabo() {
        $in = \Input::all();
        //    dd($in);
        $amount = $in['qty1'] * $in['amount1'];
        \App\Article_Resource::updateOrCreate(array('tabo_tabo_id' => $in['id'], 'article_id' => 1), array('sales_qty' => $in['qty1'], 'remain_qty' => 0, 'total_sales' => $amount,
            'unit_price' => $in['amount1']));
        $tabo = \App\TaboTabo::find($in['id']);
        $tabo->customer = $in['customer2'];
        $tabo->status = '1';
        $tabo->gross = $tabo->gross + $amount;
        $tabo->gross_profit = $tabo->gross - $tabo->expense;
        $tabo->income_deff = $tabo->gross - $tabo->expense - $tabo->price;
        $tabo->save();

        $order = new \App\Order;
        $order->date = $in['date1'];
        $date = explode("/", $in['date1']);
        $mon = $this->GetMonth($date[0]);
        $order->year = $date[2];
        $order->month = $mon;
        $order->due_date = $in['duedate1'];
        $order->dr = $in['dr1'];
        $order->customer = $in['customer2'];
        $order->type = 'delivery';
        $order->status = "pending";
        $order->total_due = $amount;
        $order->save();

        $orderlist = new \App\Orderlist;
        $orderlist->qty = $in['qty1'];
        $orderlist->order_id = $order->id;
        $orderlist->article__resource_id = $in['id'];
        $orderlist->article_id = 1;
        $orderlist->unit_price = $in['amount1'];
        $orderlist->amount = $amount;
        $orderlist->save();
        flash('Successfully Added And Updated the articles quantities.');
        return \Redirect::back();
    }

    public function AddWalk() {
        $in = \Input::all();
        //   dd($in);
    }

    public function CowInfo($id) {
        $cow = \App\Cow::find($id);
        return view('Admin.resources.own_breakdown')
                        ->with('cow', $cow);
    }

    public function ViewTaboBr($id) {
        $tabo = \App\TaboTabo::find($id);
        $articles = \App\Article::all();
        // dd($tabo);
        return view('Admin.resources.tabotabo_breakdown')
                        ->with('tabos', $tabo)
                        ->with('articles', $articles);
    }

    public function AddExpense() {
        $in = \Input::all();
        $count = count($in);
        $k = ($count - 2) / 2;
        // dd($k);
        for ($i = 0; $i < $k; $i++) {
            if ($in['name' . $i] == '' || $in['amount' . $i] == '') {
                
            } else {
                $expense = new \App\Expense;
                $expense->name = $in['name' . $i];
                $expense->amount = $in['amount' . $i];
                $expense->tabo_tabo_id = $in['id'];
                $expense->save();
                $tabo = \App\TaboTabo::find($in['id']);
                $tabo->expense = $tabo->expense + $in['amount' . $i];
                $tabo->gross_profit = $tabo->gross - $tabo->expense;
                $tabo->income_deff = $tabo->gross - $tabo->expense - $tabo->price;
                $tabo->save();
            }
        }
        return \Redirect::back();
    }

    public function AddArticle() {
        $in = \Input::all();
        //  dd($in);
        $count = count($in);
        $k = ($count - 2) / 4;
        // dd($k);
        for ($i = 0; $i < $k; $i++) {
            if ($in['article' . $i] == '' || $in['unit_price' . $i] == '' || $in['qty' . $i] == '') {
                
            } else {
                $resource = new \App\Article_Resource;
                $article = explode('_', $in['article' . $i]);
                $resource->article_id = $this->getArticleId($article['0']);
                $resource->quantity = $in['qty' . $i];
                $resource->tabo_tabo_id = $in['id'];
                $resource->sales_qty = $in['qty' . $i];
                $resource->remain_qty = 0;
                $resource->total_sales = $in['total_due' . $i];
                $resource->unit_price = $in['unit_price' . $i];
                $resource->save();
                $tabo = \App\TaboTabo::find($in['id']);
                $tabo->gross = $tabo->gross + $in['total_due' . $i];
                $tabo->gross_profit = $tabo->gross - $tabo->expense;
                $tabo->income_deff = $tabo->gross - $tabo->expense - $tabo->price;
                $tabo->save();
            }
        }
        return \Redirect::back();
    }

    public function AddBaka() {
        $in = \Input::all();
        // dd($in);
        $cow = new \App\Cow;
        $cow->date = $in['date'];
        $date = explode("/", $in['date']);
        // dd($date);
        $mon = $this->GetMonth($date[0]);
        $cow->year = $date[2];
        $cow->month = $mon;
        $cow->color = $in['color'];
        $cow->sex = $in['gender'];
        $cow->lw = $in['lw'];
        $cow->sw = $in['sw'];
        $cow->cw = $in['cw'];
        $cow->save();
        flash('Successfully Added!');
        return \Redirect::back();
    }

    public function AddParts($id) {
        $in = \Input::all();
//         dd($in);
        $count = count($in);
        // dd($count);
        $k = ($count - 2) / 2;
        // dd($k);
        for ($i = 0; $i < $k; $i++) {
            // dd($i);
            if ($in['part' . $in['cow_id'] . $i] == '' || $in['qty' . $in['cow_id'] . $i] == '') {
                
            } else {

                $cow = \App\Cow::find($id);
                $article_id = $this->getArticleId($in['part' . $in['cow_id'] . $i]);
//                dd($article_id);
                $check = \App\MainParts::where('cow_id', $id)
                                ->where('article_id', $article_id)->get();
                $var = json_decode(json_encode($check), TRUE);
                if ($var == null) {
                    $mainparts = new \App\MainParts;
                    $mainparts->cow_id = $in['cow_id'];
                    $mainparts->article = $in['part' . $in['cow_id'] . $i];
                    $mainparts->quantity = $in['qty' . $in['cow_id'] . $i];
                    $cow->mw = $cow->mw + $in['qty' . $in['cow_id'] . $i];
                    $mainparts->article_id = $article_id;
                    $mainparts->save();
                    $cow->save();
                    // dd( $mainparts);
                } else {
                    $mainparts = \App\MainParts::find($var['0']['id']);
//                    dd($mainparts);
                    $mainparts->quantity = $in['qty' . $in['cow_id'] . $i];
                    $cow->mw = $cow->mw + $in['qty' . $in['cow_id'] . $i];
                    $mainparts->save();
                    $cow->save();
                    flash('The article already exist, And now it was override by new input!');
                    return \Redirect::back();
//                    \Flash::overlay($in['part' . $in['cow_id'] . $i].' already exists','Warning');
//                    return \Redirect::back();
                }
            }
        }
        flash('Successfully Added!');
        return \Redirect::back();
    }

    //new---Cindy
    public function AddCuts($id) {
        $in = \Input::all();
//        dd($in);    
        $count = count($in);
//        dd($count);
        $k = ($count - 2) / 3;
//        dd($k);
        for ($i = 0; $i < $k; $i++) {
            if ($in['part' . $in['cow_id'] . $i] == '' || $in['qty' . $in['cow_id'] . $i] == '' || $in['type' . $in['cow_id'] . $i] == '') {
//                dd($in['part'.$in['cow_id'].$i]);
            } else {
                $art_id = $this->getArticleId($in['part' . $in['cow_id'] . $i]);
//                dd($art_id);
                $check_article_if_exist = \App\Article_Resource::where('cow_id', $id)
                                ->where('article_id', $art_id)->get();
                $var = json_decode(json_encode($check_article_if_exist), TRUE);
//                dd($var);
                if ($var == null) {
//                    dd($in['qty' . $in['cow_id'] . $i]);
                    $article_resource = new \App\Article_Resource;
                    $article_resource->cow_id = $in['cow_id'];
                    $article_resource->quantity = $in['qty' . $in['cow_id'] . $i];
                    $article_resource->remain_qty = $in['qty' . $in['cow_id'] . $i];
                    $article_resource->type = $in['type' . $in['cow_id'] . $i];
                    $article_resource->article_id = $art_id;
                    $article_resource->save();
//                dd($article);
                } else {
                    $article_resource = \App\Article_Resource::find($var['0']['id']);
//                    dd($article_resource);
                    $article_resource->quantity = $in['qty' . $in['cow_id'] . $i];
                    $article_resource->remain_qty = $in['qty' . $in['cow_id'] . $i];
                    $article_resource->type = $in['type' . $in['cow_id'] . $i];
                    $article_resource->save();
                }//               
            }
        }
        flash('Successfully Added!');
        return \Redirect::back();
    }

    public function getArticleId($name) {
        $id = \App\Article::where('name', $name)->first();
        return $id['id'];
        //return $id;
    }

    public function GetMonth($mon) {
        //$month;
        if ($mon == "01") {
            $month = "January";
        } elseif ($mon == "02") {
            $month = "Febuary";
        } elseif ($mon == "03") {
            $month = "March";
        } elseif ($mon == "04") {
            $month = "April";
        } elseif ($mon == "05") {
            $month = "May";
        } elseif ($mon == "06") {
            $month = "June";
        } elseif ($mon == "07") {
            $month = "July";
        } elseif ($mon == "08") {
            $month = "August";
        } elseif ($mon == "09") {
            $month = "September";
        } elseif ($mon == "10") {
            $month = "October";
        } elseif ($mon == "11") {
            $month = "November";
        } elseif ($mon == "12") {
            $month = "December";
        } else {
            $month = null;
        }
        return $month;
    }

    public function ViewReports($id) {
        $articles = \App\Article_Resource::where('outsource_id', '=', $id)->get();
        $history = \App\Payment_History::where('outsource_i', '=', $id)->get();
        return view('Admin.resources.outsource_reports')
                        ->with('history', $history)
                        ->with('articles', $articles);
    }

}
