<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

class OrderController extends Controller
{

    /**
     * Created by PhpStorm.
     * User: Sui~yans
     * Date: 5/22/2016
     * Time: 5:56 PM
     */


    public function AddOrder()
    {
        $order = \Input::all();
        $Order = new \App\Order;
        //  dd($order);
        $check = array_slice($order, 0, -1);
        $count = count($check);
        $k = ($count - 9) / 7;
      //  dd($k);
        if ($order['radBtn'] == '1') {
            if ($order['deliverto'] == '' || $order['dr'] == '') {
                \Flash::warning('Missing required inputs.');
                return \Redirect::back();
            }
            $Order->date = $order['date1'];
            $date = explode("/", $order['date1']);
            // dd($date);
            $mon = $this->GetMonth($date[0]);
            $Order->year = $date[2];
            $Order->month = $mon;
            $Order->dr = $order['dr'];
            $Order->customer = $order['deliverto'];
            $Order->due_date = $order['duedate'];
            $Order->type = 'delivery';
            $Order->status = 'pending';
            $Order->balance = $order['Tamount'];

        } else if ($order['radBtn'] == '0') {
//
            if ($order['or']=='' || $order['sold'] == '') {
                \Flash::warning('Missing required inputs.');
                return \Redirect::back();
            }
            $Order->date = $order['date3'];
            $date = explode("/", $order['date3']);
            // dd($date);
            $mon = $this->GetMonth($date[0]);
            $Order->year = $date[2];
            $Order->month = $mon;
            $Order->or = $order['or'];
            $Order->customer = $order['sold'];
            $Order->type = 'walked-in';
            $Order->status = 'paid';
            $Order->bank = 'cash';
            $Order->amount_paid = $order['Tamount'];
        }
        for ($i = 0; $i < $k; $i++) {
            $article_id_to = $this->getArticleId($order['articleD' . $i]);
            $article_id_from = $this->getArticleId($order['articlesFrD' . $i]);
            if ($order['type' . $i] == 'farm') {
                $cow_article = \App\Article_Resource::where('cow_id', $order['id' . $i])->where('article_id', $article_id_from)->get();
            } elseif ($order['type' . $i] == 'tabo') {
                $cow_article = \App\Article_Resource::where('tabo_tabo_id', $order['id' . $i])->where('article_id', $article_id_from)->get();
            } elseif ($order['type' . $i] == 'vendor') {
                $cow_article = \App\Article_Resource::where('outsource_id', $order['id' . $i])->where('article_id', $article_id_from)->get();
            }
            $check1 = json_decode(json_encode($cow_article), true);
            //  dd($check1);
            if ($check1 == null) {
                \Flash::warning('No articles found for the specific Body Number');
                return \Redirect::back();
            } else {
                foreach ($cow_article as $article) {
                    //  dd($article->sales_qty);
                    $article->sales_qty = $article->sales_qty + $check['qtyD' . $i];
                    $article->remain_qty = $article->remain_qty - $check['qtyD' . $i];
                    $article->total_sales = $article->total_sales + $check['TotalPD' . $i];
                    $article->save();
                }
                $Order->total_due = $order['Tamount'];
                $Order->save();
                $orderlist = new \App\Orderlist;
                $orderlist->order_id = $Order->id;
                $orderlist->qty = $check['qtyD' . $i];
                $orderlist->amount = $check['TotalPD' . $i];
                $orderlist->unit_price = $check['unitPD' . $i];
                $orderlist->article_id = $article_id_to;
                $orderlist->from_article = $order['articlesFrD' . $i];
                $orderlist->article__resource_id = $check1['0']['id'];
                $orderlist->save();
            }
        }
        flash('Successfully Recorded!');
        return \Redirect::back();
    }

    public
    function AddPurchase()
    {
        $purchase = \Input::all();
        //dd($purchase);

        $buy = new \App\Outsource;
        $buy->date = $purchase['datePurchased'];
        $date = explode("/", $purchase['datePurchased']);
        // dd($date);
        $mon = $this->GetMonth($date[0]);
        $buy->year = $date[2];
        $buy->month = $mon;
        $buy->ordered_from = $purchase['supplier'];
        $buy->dr = $purchase['dr#'];
        $buy->amount = $purchase['Tamnt'];
        $buy->balance = $purchase['Tamnt'];
        $buy->due_date = $purchase['dueDate'];
        $buy->status = 'pending';
        $buy->save();
        $outsource_id = $buy->id;
        //dd($outsource_id);

        //$count = count($purchase);
        $articles = array_slice($purchase, 0, -1);
        $count = count($articles);
        $k = ($count - 5) / 4;

        for ($i = 0; $i < $k; $i++) {
            $addArticle = new \App\Article_Resource;
            $addArticle->outsource_id = $outsource_id;
            //$articleNew = $purchase['articleV'.$i];
            $article_id = $this->getArticleId($purchase['articleV' . $i]);
            $addArticle->article_id = $article_id;
            $addArticle->quantity = $purchase['qtyV' . $i];
            $addArticle->unit_price = $purchase['unitPV' . $i];
            $addArticle->total_price = $purchase['TotalPV' . $i];
            $addArticle->remain_qty = $purchase['qtyV' . $i];
            //dd($article_id);
            $addArticle->save();
        }
        // dd($article_id);

        flash('Successfully Added!');
        return \Redirect::back();
    }

    public
    function Add_outsourceOrder()
    {
        $purchase = \Input::all();
        //dd($purchase);

        $buy = new \App\Order;
        $buy->date = $purchase['date1'];
        $date = explode("/", $purchase['date1']);
        // dd($date);
        $mon = $this->GetMonth($date[0]);
        $buy->year = $date[2];
        $buy->month = $mon;
        $buy->customer = $purchase['deliver'];
        $buy->dr = $purchase['dr'];
        $buy->total_due = $purchase['Tamount'];
        $buy->due_date = $purchase['date2'];
        $buy->balance = $purchase['Tamount'];
        $buy->status = 'pending';
        $buy->type = 'delivery';
        $buy->save();
        $order_id = $buy->id;
        //dd($outsource_id);

        //$count = count($purchase);
        $articles = array_slice($purchase, 0, -1);
        $count = count($articles);
        $k = ($count - 6) / 4;

        for ($i = 0; $i < $k; $i++) {
            $addArticle = new \App\Orderlist;
            $addArticle->order_id = $order_id;
            //$articleNew = $purchase['articleV'.$i];
            $article_id = $this->getArticleId($purchase['articleD' . $i]);
            $addArticle->article_id = $article_id;
            $addArticle->qty = $purchase['qtyD' . $i];
            $addArticle->unit_price = $purchase['unitPD' . $i];
            $addArticle->amount = $purchase['TotalPD' . $i];
            $qtyLeft_new = \App\Article_Resource::where('outsource_id', $purchase['id'])->where('article_id', $article_id)->get();
            //dd($qtyLeft_new);
            foreach ($qtyLeft_new as $count1) {
                $count1->remain_qty = $count1->remain_qty - $purchase['qtyD' . $i];
                $addArticle->article__resource_id = $count1->id;
                $count1->save();
            }
            $addArticle->from_article = $purchase['articleD' . $i];
            $addArticle->save();
            //dd($qtyLeft_new->remain_qty);
        }
        // dd($article_id);


        flash('Successfully Added!');
        return \Redirect::back();
    }

    public
    function GetMonth($mon)
    {
        //$month;
        if ($mon == "01") {
            $month = "January";
        } elseif ($mon == "02") {
            $month = "Febuary";
        } elseif ($mon == "03") {
            $month = "March";
        } elseif ($mon == "04") {
            $month = "April";
        } elseif ($mon == "05") {
            $month = "May";
        } elseif ($mon == "06") {
            $month = "June";
        } elseif ($mon == "07") {
            $month = "July";
        } elseif ($mon == "08") {
            $month = "August";
        } elseif ($mon == "09") {
            $month = "September";
        } elseif ($mon == "10") {
            $month = "October";
        } elseif ($mon == "11") {
            $month = "November";
        } elseif ($mon == "12") {
            $month = "December";
        } else {
            $month = null;
        }
        return $month;

    }

    public
    function getArticleId($name)
    {
        $id = \App\Article::where('name', $name)->first();
        return $id['id'];
        //return $id;
    }

}