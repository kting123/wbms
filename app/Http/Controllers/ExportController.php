<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

class ExportController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth');
    }

    public function ExportOR() {
        $in = \Input::all();
        //dd($in);
        if ($in['all'] == '1') {
            $data1 = \App\Order::where('status', 'paid')->orderBy('or', 'desc')->get();
            $label = "From the beginning";
        } else if ($in['type'] == 'WalkedIn&Delivery') {
            $data1 = \App\Order::where('year', $in['year'])
                            ->where('month', $in['month'])
                            ->where('status', 'paid')->orderBy('or', 'desc')->get();
            $label = "WalkedIn&Delivery" . $in['month'] . '-' . $in['year'];
        } else if ($in['type'] == 'WalkedIn') {
            $data1 = \App\Order::where('year', $in['year'])
                            ->where('month', $in['month'])
                            ->where('status', 'paid')
                            ->where('type', 'WalkedIn')->orderBy('or', 'desc')->get();
            $label = "WalkedIn - " . $in['month'] . '-' . $in['year'];
        } else if ($in['type'] == 'Delivery') {
            $data1 = \App\Order::where('year', $in['year'])
                            ->where('month', $in['month'])
                            ->where('type', 'Delivery')
                            ->where('status', 'paid')->orderBy('or', 'desc')->get();
            $label = "Delivery - " . $in['month'] . '-' . $in['year'];
        }
        if ($in['type'] == 'WalkedIn') {
            foreach ($data1 as $key => $datus) {
                $data[$key]['Date'] = $datus['date'];
                $data[$key]['OR'] = $datus['or'];
                $data[$key]['Received From'] = $datus['customer'];
                $data[$key]['Amount'] = '₱' . number_format($datus['total_due'], 2);
                //  $data[$key][$datus['or']] = $datus['date'];
                // dd($data);
            }
        } elseif ($in['type'] == 'Delivery') {
            foreach ($data1 as $key => $datus) {
                $data[$key]['Date'] = $datus['date'];
                $data[$key]['OR'] = $datus['or'];
                $data[$key]['Received From'] = $datus['customer'];
                $data[$key]['Amount'] = '₱' . number_format($datus['total_due'], 2);
                $data[$key]['Check#'] = $datus['check'];
                $data[$key]['Bank'] = $datus['bank'];
                $data[$key]['Date Of Check'] = $datus['date_of_check'];
                // dd($data);
            }
        } else {
            foreach ($data1 as $key => $datus) {
                $data[$key]['Date'] = $datus['date'];
                $data[$key]['OR'] = $datus['or'];
                $data[$key]['Received From'] = $datus['customer'];
                $data[$key]['Amount'] = '₱' . number_format($datus['total_due'], 2);
                $data[$key]['Check#'] = $datus['check'];
                $data[$key]['Bank'] = $datus['bank'];
                $data[$key]['Date Of Check'] = $datus['date_of_check'];
                $data[$key]['type'] = $datus['type'];
                // dd($data);
            }
        }
        $title = "OR FOR PBF CATTLE RAISERS - " . $label;
        $this->generateExcel($data, $title);
        //    dd($data);
    }

    
     public function ExportDR() {
        $in = \Input::all();
        //  dd($in);
        if ($in['all'] == '1') {
            $data1 = \App\Order::where('type', '=', 'delivery')
                            ->orderBy('id', 'desc')->get();
            $label = "Beginning Of time";
        } else {
            $data1 = \App\Order::where('year', '=', $in['year'])
                            ->where('month', '=', $in['month'])
                            ->where('type', '=', 'delivery')
                            ->orderBy('id', 'desc')->get();
            $label = $in['month'] . '/' . $in['year'];
        }

//dd($data1);

        \Excel::create("DELIVERY RECEIPT PBF CATTLE RAISER" . $label, function ($excel) use ($data1) {
            $excel->sheet('Sheet 1', function ($sheet) use ($data1) {
                $sheet->setWidth(array(
                    'A' => 30,
                    'B' => 20,
                    'C' => 20,
                    'D' => 25,
                    'E' => 20,
                    'F' => 20,
                    'I' => 20,
                    'H' => 20,
                    'G' => 20
                ));
                //$sheet->setWrapText('true');
                $sheet->loadView('Admin.forExports.exportdr')->with('order', $data1);
            });
        })->export('xls');
    }

    
    public function ExportOutsource() {
        $in = \Input::all();
        //  dd($in);
        if ($in['all'] == '1') {
            $data1 = \App\Outsource::where('tsk', '=', '0')
                            ->orderBy('id', 'desc')->get();
            $label = "Beginning Of time";
        } else {
            $data1 = \App\Outsource::where('year', '=', $in['year'])
                            ->where('month', '=', $in['month'])
                            ->orderBy('id', 'desc')->get();
            $label = $in['month'] . '/' . $in['year'];
        }
//dd($data1);
        foreach ($data1 as $key => $datus) {
            $data[$key]['Date'] = $datus['date'];
            $data[$key]['OR#'] = $datus['or'];
            $data[$key]['DR#'] = $datus['dr'];
            $data[$key]['Ordered From'] = $datus['ordered_from'];
            $data[$key]['Amount'] = '₱' . number_format($datus['amount'], 2);
            ;
            $data[$key]['Status'] = $datus['status'];
            $data[$key]['Due Date'] = $datus['due_date'];
        }

        //    dd($data);
        $title = "From Other Vendor - " . $label;
        $this->generateExcel($data, $title);
    }
    public function ExportTabo() {
        $in = \Input::all();

        if ($in['all'] == '1') {
            $data = \App\TaboTabo::where('tsk', '=', '0')->orderBy('id', 'desc')->get();

            $label = "Beginning Of time";
        } else {
            $data = \App\TaboTabo::whereBetween('date_of_purchase', [$in['date_from'], $in['date_to']])->orderBy('id', 'desc')->get();
            $label = $in['date_from'] . " to " . $in['date_to'];
        }
        
        \Excel::create("Tabo Tabo Report ( " . $label . " )", function ($excel) use ($data) {
            foreach ($data as $tabo) {

                $excel->sheet('Body #' . $tabo['id'], function ($sheet) use ($tabo) {

                    $sheet->setWidth(array(
                        'A' => 30,
                        'B' => 20,
                        'C' => 20,
                        'D' => 20,
                        'E' => 20,
                        'F' => 20,
                        'I' => 20,
                        'H' => 20,
                        'G' => 20
                    ));
                    $sheet->loadView('Admin.forExports.export_FullTaboInfo')->with('tabos', $tabo);
                });
            }
        })->export('xls');
 
    }
    public function ExportOwn() {
        $in = \Input::all();
        if ($in['all'] == '1') {
            $data1 = \App\Cow::where('tsk', '=', '0')
                            ->orderBy('id', 'desc')->get();
            $label = "Beginning Of time";
        } else {
            $data1 = \App\Cow::where('year', '=', $in['year'])
                            ->where('month', '=', $in['month'])
                            ->orderBy('id', 'desc')->get();
            $label = $in['month'] . '/' . $in['year'];
        }
//dd($data1);
        foreach ($data1 as $key => $datus) {
            $data[$key]['Date Added'] = $datus['date'];
            $data[$key]['Body#'] = $datus['id'];
            $data[$key]['Color'] = $datus['color'];
            $data[$key]['Sex'] = $datus['sex'];
            $data[$key]['Live Weight'] = $datus['lw'];
            $data[$key]['Slaughter Weight'] = $datus['sw'];
            //  $data[$key][$datus['or']] = $datus['date'];
            // dd($data);
        }

        //    dd($data);
        $title = "From Farm - " . $label;
        $this->generateExcel($data, $title);
    }

    public function printCowReport($id) {
        $cow = \App\Cow::find($id);
        // dd($cow);
        \Excel::create("from Farm - Body Number : " . $cow['id'] . "/" . $cow['date'], function ($excel) use ($cow) {
            $excel->sheet('Sheet 1', function ($sheet) use ($cow) {
                $sheet->setWidth(array(
                    'A' => 30,
                    'B' => 40,
                    'C' => 20,
                    'D' => 20,
                    'E' => 20,
                    'F' => 10,
                    'I' => 10,
                    'H' => 10,
                    'G' => 10
                ));
                $sheet->loadView('Admin.forExports.export_cowInfo')->with('cow', $cow);
            });
        })->export('xls');
    }

    public function printTaboReport($id) {
        $tabo = \App\TaboTabo::find($id);
        // dd($cow);
        \Excel::create("Tabo Tabo - Body Number : " . $tabo['id'] . "/" . $tabo['date'], function ($excel) use ($tabo) {


            $excel->sheet('Sheet 1', function ($sheet) use ($tabo) {
                $sheet->setWidth(array(
                    'A' => 30,
                    'B' => 20,
                    'C' => 20,
                    'D' => 20,
                    'E' => 20,
                    'F' => 20,
                    'I' => 20,
                    'H' => 20,
                    'G' => 20
                ));
                $sheet->loadView('Admin.forExports.export_taboInfo')->with('tabos', $tabo);
            });
        })->export('xls');
    }

    /*   end new function */

    public function printTaboReportFull() {
        $tabo = \App\TaboTabo::orderBy('id', 'desc')->get();
//        $count = count($tabo);
//         dd($cow);
//        dd($tabo);
        \Excel::create("Tabo Tabo Report ", function ($excel) use ($tabo) {
            foreach ($tabo as $tabo) {
                $excel->sheet('Body Number ' . $tabo['id'], function ($sheet) use ($tabo) {
                    $sheet->setWidth(array(
                        'A' => 30,
                        'B' => 20,
                        'C' => 20,
                        'D' => 20,
                        'E' => 20,
                        'F' => 20,
                        'I' => 20,
                        'H' => 20,
                        'G' => 20
                    ));
                    $sheet->loadView('Admin.forExports.export_FullTaboInfo')->with('tabos', $tabo);
                });
            }
        })->export('xls');
    }
}