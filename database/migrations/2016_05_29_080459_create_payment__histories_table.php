<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentHistoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payment__histories', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('outsource_i');
			$table->integer('order_id');
			$table->string('date_of_check');
			$table->string('bank');
			$table->string('check');
			$table->double('amount_paid');
			$table->double('balance');
			$table->integer('or');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payment__histories');
	}

}
