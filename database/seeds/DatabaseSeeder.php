<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 49; $i > 96; $i++) {
            $id = $i;
            $date = '05/29/2016';
            $qty = '0';
            $history = new \App\Article_History;
            $history->article_id = $id;
            $history->qty_remain = $qty;
            $history->date = $date;
            $history->save();
//            DB::table('article__histories')->insert([
//                'date' => $date ,
//                'article_id' => $id,
//                'qty_remain'=>$qty,
//            ]);

            //$orderlist = new \App\Orderlist;
//            $order = new \App\Order;
//            $order->date = $date;
//            $order->year = '2016';
//            $order->month = 'April';
//            $order->dr = random_int(100,1000);
//            $order->customer = str_random(10);
//            $order->type = 'delivery';
//            $order->status = 'pending';
//            $order->due_date = $due_date;
//            $order->total_due = random_int(1000, 10000);
//            $order->save();
//            $orderlist = new \App\Orderlist;
//            $orderlist->qty = random_int(1, 10);
//            $orderlist->order_id = $order->id;
//            $orderlist->unit_price = 1000 * random_int(1, 10);
//            $orderlist->amount = 200 * random_int(1, 10);
//            $orderlist->article_id = random_int(1, 5);
//            $orderlist->save();
        }
    }
}
