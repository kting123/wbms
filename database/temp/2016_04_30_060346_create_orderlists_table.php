<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderlistsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orderlists', function(Blueprint $table)
		{
			$table->increments('id');
                        $table->integer('order_id');
                        $table->double('qty');
                        $table->double('amount');
                        $table->double('unit_price');
                        $table->integer('article_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orderlists');
	}

}
