<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleHistoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('article__histories', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('article_id');
			$table->double('qty_remain');
			$table->double('qty_sale');
			$table->double('sale_amount');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('article__histories');
	}

}
