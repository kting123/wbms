<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('orders', function(Blueprint $table) {
            $table->increments('id');
            $table->string('date');
            $table->integer('customer_id');
            $table->integer('or')->nullable();
            $table->integer('dr')->nullable();
            $table->string('due_date');
            $table->string('status');
            $table->string('type');
            $table->integer('check')->nullable();
            $table->string('date_of_check');
            $table->string('bank');
            $table->string('month');
            $table->string('year');
            $table->double('total_due');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('orders');
    }

}
