@extends('app')

@section('content')
    <style>
        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
            padding: 1px;
        }
    </style>
    <div class="container">
        <div class="table-responsive">
            <td><h5><b>History of Payments</b></h5></td>
            <table class="table table-bordered col-lg-12 col-md-12 col-xs-12">
                <thead style="border-bottom:solid 2px;">
                <th></th>
                <th>Date of Payment</th>
                <th>Amount (Php)</th>
                <th>Bank Name</th>
                <th>Check No.</th>
                <th>Balance (Php)</th>
                </thead>
                <tbody>
                @foreach($history as $histories)
                    <tr>
                        <td></td>
                        <td>{{$histories['date_of_check']}}</td>
                        <td>{{number_format($histories->amount_paid,2)}}</td>
                        <td>{{$histories->bank}}</td>
                        <td>{{$histories->check}}</td>
                        <td>{{number_format($histories->balance,2)}}</td>
                    </tr>
                @endforeach
                <tr style="border-top:solid 2px;">
                    <td style="background-color: #c6cad5;"><h5><b>Current Balance</b></h5></td>
                    <td style="background-color: #c6cad5;"></td>
                    <td style="background-color: #c6cad5;"></td>
                    <td style="background-color: #c6cad5;"></td>
                    <td style="background-color: #c6cad5;"></td>
                    <td style="background-color: #c6cad5;">
                        <b><br>{{number_format($article['outsource']['balance'],2)}}</b></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered col-lg-12 col-md-12 col-xs-12">
                <thead style="border-bottom:solid 2px;">
                <th></th>
                <th>Article Name</th>
                <th>Quantity (kg)</th>
                <th>Unit Price (Php)</th>
                <th>Total Price (Php)</th>
                <th>Quantity Left (kg)</th>
                <th>Total Sales (Php)</th>
                <th>Total (Php)</th>
                <th>Balance (Php)</th>
                <th>Action</th>
                </thead>
                <tbody>
                <tr>
                    <td><h5><b>From Supplier</b></h5></td>
                </tr>
                @foreach($articles as $article)
                    <tr>
                        <td></td>
                        <td>{{$article['article']['name']}} <label
                                    class="label label-{{Auth::user()->labels}}">{{$article['id']}}</label>
                        </td>
                        <td>{{$article->quantity}}</td>
                        <td>{{number_format($article->unit_price,2)}}</td>
                        <td>{{number_format($article->total_price,2)}}</td>
                    </tr>
                @endforeach
                <tr style="border-top:solid 2px; border-bottom:solid 10px;">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><b>{{number_format($article['outsource']['amount'],2)}}</b></td>
                    <td><b>{{number_format($article['outsource']['balance'],2)}}</b></td>
                    <td>
                        @if($article['outsource']['status'] == 'pending')
                            <button class="btn btn-success btn-group-lg" data-toggle="modal"
                                    data-target="#myModal">
                                Pay Order
                            </button>
                        @elseif($article['outsource']['status'] == 'partial')
                            <button class="btn btn-success btn-group-xs" data-toggle="modal"
                                    data-target="#myModal">
                                Pay Order
                            </button>
                        @else
                            <label class="label label-success">Payment Posted</label>
                        @endif
                    </td>
                </tr>
                <tr></tr>
                <tr></tr>
                <tr>
                    <td><h5><b>Sales</b></h5></td>
                </tr>
                <?php $i = 0; $total = 0; ?>
                @foreach($articles as $article)
                    <tr>
                        <td></td>
                        <td>{{$article['article']['name']}} <label
                                    class="label label-{{Auth::user()->labels}}">{{$article['id']}}</label>
                        </td>

                        <td>
                            @foreach($article['orderlist'] as $article1)
                                {{$article1['qty']}}
                                <br>
                            @endforeach
                        </td>
                        <td>
                            @foreach($article['orderlist'] as $article1)
                                {{number_format($article1['unit_price'],2)}}
                                <br>
                            @endforeach
                        </td>
                        <td>
                            @foreach($article['orderlist'] as $article1)
                                {{number_format($article1['amount'],2)}}
                                <br>
                            @endforeach
                        </td>

                        <td>{{$article->remain_qty}}</td>
                        <td>{{number_format($article->total_sales,2)}}</td>
                        <?php $i = $i + $article->total_sales; ?>
                    </tr>
                @endforeach
                <tr style="border-top:solid 2px; border-bottom:solid 10px;">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><b>{{number_format($i,2)}}</b></td>
                </tr>
                <tr></tr>
                <tr></tr>
                <tr>
                    <?php $total = $i - $article['outsource']['amount'];  ?>
                    <td><h5><b>Gross Profit</b></h5></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    @if($total>0)
                        <td style="background-color: #c0f080;"><b><br>{{number_format($total,2)}}</b></td>
                    @elseif($total<0)
                        <td style="background-color: #FFA07A;"><b><br>{{number_format($total,2)}}</b></td>
                    @else
                        <td style="background-color: #f3d17a;"><b><br>{{number_format($total,2)}}</b></td>
                    @endif
                </tr>
                </tbody>
            </table>
        </div>
    </div>