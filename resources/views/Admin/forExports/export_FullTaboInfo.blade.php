<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 col-lg-2 col-sm-2 col-lg-offset-1">
            <div class="col-md-8 col-sm-8 col-lg-8">
                <div class="table table-responsive">
                    <table class="table table-bordered">
                        <tbody>
                            <tr style="border-top:solid 2px;">
                                <td>Date : <b>{{$tabos['date_of_purchase']}}</b></td>
                                <td>Body# : <b>{{$tabos['id']}}</b></td>
                                <td>Gender : <b>{{$tabos['sex']}}</b></td>
                                <td>Area : <b>{{$tabos['area']}}</b></td>
                                <td>Color : <b>{{$tabos['color']}}</b></td>
                                <td>Delivered To : <b>{{$tabos['customer']}}</b></td>
                            </tr>
                            <tr style="border-top:solid 2px;">
                                <td></td>                            
                                <td></td>
                                <td>LW : <b>{{$tabos['lw']}}</b> Kgs.</td>
                                <td>SW : <b>{{$tabos['sw']}}</b> Kgs.</td>
                                <td> MW : <b>{{$tabos['mw']}}</b> Kgs.</td>
                                <td>Owner : <b>{{$tabos['owner']}}</b></b></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-8 col-sm-8 col-lg-8">
                <div class="table table-responsive">
                    <table class="table table-bordered">
                        <thead style="padding: 0px 0px 0px 0px">
                        <th></th>
                        <th>Article <br>Expense</th>
                        <th>Quantity<br>(Kls)</th>
                        <th>Unit Price</th>
                        <th>Sales</th>
                        <th>Total</th>
                        </thead>
                        <tbody>

                            <tr>
                                <td><b>Sales : </b></td>
                            </tr>
                            @foreach($tabos['article_resources'] as $article)
                            <tr>
                                <td></td>
                                <td>{{$article['article']['name']}}  ({{$article['id']}})</td>
                                <td>{{$article['sales_qty']}}</td>
                                <td>{{number_format($article['unit_price'],2)}}</td>
                                <td>{{number_format($article['total_sales'],2)}}</td>
                            </tr>
                            @endforeach
                            <tr style="border-top:solid 2px;">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                        <center><b>{{number_format($tabos['gross'],2)}}</b></center>
                        </td>
                        </tr>


                        <tr>
                            <td><b>Expenses : </b></td>
                        </tr>

                        @foreach($tabos['expenses'] as $expense)
                        <tr>
                            <td></td>
                            <td>{{$expense['name']}}</td>
                            <td></td>
                            <td></td>
                            <td>{{number_format($expense['amount'],2)}}</td>
                            <td></td>
                        </tr>
                        @endforeach

                        <tr style="border-top:solid 2px;">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="border-bottom: solid 2px;">
                        <center><b>{{number_format($tabos['expense'],2)}}</b></center>
                        </td>
                        </tr>
                        <tr></tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><b>Gross Profit : </b></td>
                            <td style="border-bottom:solid 5px;">
                        <center>{{number_format($tabos['gross_profit'],2)}}</center>
                        </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><b>Price : </b></td>
                            <td style="border-bottom:solid 5px;">
                        <center>{{number_format($tabos['price'],2)}}</center>
                        </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><b>Income/Diff : </b></td>
                            <td style="border-bottom:solid 5px;">
                        <center><b>{{number_format($tabos['income_deff'],2)}}</b></center>
                        </td>
                        </tr>
                        </tbody>

                    </table>
                </div>

            </div>

        </div>

    </div>





