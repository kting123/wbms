@extends('app')

@section('content')
<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: 1px;
    }
    .table td {
      font-size : 16px;
    }
    .table th {
      font-size : 16px;
    }
</style>
<div class="container">
    <div class="row">
        <div class="panel panel-{{Auth::user()->panels}}">
            <div class="panel-heading">
                <h4>Outsourcing Reports</h4>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered col-lg-12 col-md-12 col-xs-12">
                        <thead style="border-bottom:solid 2px;">
                        <th></th>
                        <th>Article Name</th>
                        <th>Quantity (kg)</th>
                        <th>Unit Price (Php)</th>
                        <th>Total Price (Php)</th>
                        <th>Quantity Left (kg)</th>
                        <th>Total Sales (Php)</th>
                        <th>Total (Php)</th>
                        <!-- <th>Balance (Php)</th> -->
                        </thead>
                        <tbody>
                            <tr><td><h5><b>From Supplier</b></h5></td></tr>
                            @foreach($articles as $article)
                            <tr>
                                <td></td>
                                <td>{{$article['article']['name']}} <label class = "label label-{{Auth::user()->labels}}">{{$article['id']}}</label></td>
                                <td>{{$article->quantity}}</td>
                                <td>{{number_format($article->unit_price,2)}}</td>
                                <td>{{number_format($article->total_price,2)}}</td>
                                <td></td>
                                <td></td>
                                <td></td>

                            </tr>
                            @endforeach
                            <tr style="border-top:solid 2px; border-bottom:solid 2px;">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><b>{{number_format($article['outsource']['amount'],2)}}</b> </td>
                                <!-- <td><b>{{number_format($article['outsource']['balance'],2)}}</b></td> -->
                            </tr>
                            <tr></tr>
                            <tr></tr>
                            <tr ><td><h5><b>Sales</b></h5></td></tr>
                            <?php $i = 0;
                            $total = 0; ?>
                            @foreach($articles as $article)
                            <tr>
                                <td></td>
                                <td>{{$article['article']['name']}} <label class = "label label-{{Auth::user()->labels}}">{{$article['id']}}</label> </td>

                                <td>
                                    @foreach($article['orderlist'] as $article1)
                                    {{$article1['qty']}}
                                    <br>
                                    @endforeach
                                </td>
                                <td>
                                    @foreach($article['orderlist'] as $article1)
                                    {{number_format($article1['unit_price'],2)}}
                                    <br>
                                    @endforeach
                                </td>
                                <td>
                                    @foreach($article['orderlist'] as $article1)
                                    {{number_format($article1['amount'],2)}}
                                    <br>
                                    @endforeach
                                </td>

                                <td>{{$article->remain_qty}}</td>
                                <td>{{number_format($article->total_sales,2)}}</td>
                                <td></td>

<?php $i = $i + $article->total_sales; ?>
                            </tr>
                            @endforeach
                            <tr style="border-top:solid 1px; border-bottom:solid 2px;">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><b>{{number_format($i,2)}}</b></td>


                            </tr>
                            <tr></tr>
                            <tr></tr>
                            <tr >
<?php $total = $i - $article['outsource']['amount']; ?>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><h4><b>Gross Profit</b></h4></td>
                                @if($total>0)
                                <td style="background-color: #c0f080;"><b><br>{{number_format($total,2)}}</b></td>
                                @elseif($total<0)
                                <td style="background-color: #FFA07A;"><b><br>{{number_format($total,2)}}</b></td>
                                @else
                                <td style="background-color: #f3d17a;"><b><br>{{number_format($total,2)}}</b></td>
                                @endif

                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @if($article['outsource']['status'] == 'pending')
    <button class="btn btn-success btn-lg" data-toggle="modal" data-target="#myModal">
        Pay Order
    </button>
    @elseif($article['outsource']['status'] == 'partial')
    <button class="btn btn-success btn-lg" data-toggle="modal" data-target="#myModal">
        Pay Order
    </button>
    @else
    <label class="label label-success">Payment Posted</label>
    @endif
</div>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Pay Order</h4>
            </div>
            <div class="modal-body">
                <form type="hidden" method="post" action="./post_vendorPayment{{$article['outsource']['id']}}" id="form1"/>
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                <div class="table-responsive">
                    <td><h5><b>History of Payments</b></h5></td>
                    <table class="table table-bordered col-lg-12 col-md-12 col-xs-12">
                        <thead style="border-bottom:solid 2px;">
                        <th></th>
                        <th>Date of Payment</th>
                        <th>Amount (Php)</th>
                        <th>OR #</th>
                        <th>Bank Name</th>
                        <th>Check No.</th>
                        <th>Balance (Php)</th>
                        </thead>
                        <tbody>
                            @foreach($history as $histories)
                            <tr>
                                <td></td>
                                <td>{{$histories['date_of_check']}}</td>
                                <td>{{number_format($histories->amount_paid,2)}}</td>
                                <td>{{$histories->or}}</td>
                                <td>{{$histories->bank}}</td>
                                <td>{{$histories->check}}</td>
                                <td>{{number_format($histories->balance,2)}}</td>
                            </tr>
                            @endforeach
                            <tr style="border-top:solid 2px;">
                                <td style="background-color: #c6cad5;"><h5><b>Current Balance</b></h5></td>
                                <td style="background-color: #c6cad5;"></td>
                                <td style="background-color: #c6cad5;"></td>
                                <td style="background-color: #c6cad5;"></td>
                                <td style="background-color: #c6cad5;"></td>
                                <td style="background-color: #c6cad5;"></td>
                                <td style="background-color: #c6cad5;"><b><br>{{number_format($article['outsource']['balance'],2)}}</b></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-12">
                    <label class="checkbox-inline" style="margin-left: 10px;"> <input type="checkbox" id="cheque"> Pay in Cheque</input>
                    </label>
                </div>
                <div class="form-group col-lg-12 col-md-12">

                    <div class="col-md-4 col-lg-4 col-xs-12">
                        <label>Amount</label>
                        <input placeholder="" value="{{$article['outsource']['balance']}}" name="amount" class="form-control">
                        </input>
                    </div>
                    <div class="col-md-4 col-lg-4col-xs-12">
                        <label>OR No.</label>
                        <input placeholder="or" name="or" class="form-control">
                        </input>
                    </div>
                    <div class="col-md-4 col-lg-4 col-xs-12">
                        <label>Date Of Payment</label>
                        <input placeholder="" name="dateofpayment" id="date2" class="form-control">
                        </input>
                    </div>
                    <div class=" col-md-4 col-lg-4 col-xs-12" id="bank" hidden>
                        <label>Bank</label>
                        <input placeholder="Bank Name" name="bank" class="form-control" id="banks" disabled>
                        </input>
                    </div>
                    <div class=" col-md-4 col-lg-4 col-xs-12" id="checkNum" hidden>
                        <label>Cheque No.</label>
                        <input placeholder="check no" name="checkno" id="check" class="form-control" disabled>
                        </input>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span>
                    Cancel
                </button>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-ok"></span> Post
                    Payment
                </button>
            </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    document.getElementById("arrow").innerHTML = "  Meatshop";
    document.getElementById("arrow1").innerHTML = "  Outsource ID {{$article['outsource']['id']}}";
    $(document).ready(function () {
        $(function () {
            $('#date2').datepicker();
        });
    });

    $(document).ready(function () {
        $('#cheque').click(function () {
            if ($(this).is(":checked")) {
                // check = 1;
                $('#bank').show();
                $('#checkNum').show();
                document.getElementById("banks").disabled = false;
                document.getElementById("check").disabled = false;

            }
            else {
                $('#bank').hide();
                $('#checkNum').hide();
                document.getElementById("banks").disabled = true;
                document.getElementById("check").disabled = true;
                //  check = 0;
            }
        });
    });

</script>

@endsection
