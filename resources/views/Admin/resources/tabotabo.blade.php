@extends('app')

@section('content')
    <style>
        #arrow1 {
            font-weight: bold;
        }

        .results tr[visible='false'],
        .no-result {
            display: none;
        }

        .results tr[visible='true'] {
            display: table-row;
        }

        .counter {
            padding: 8px;
            color: #ccc;
        }
    </style>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="pull-left col-md-3 col-lg-3 col-sm-3" style="padding-left:0;">
                    <input class="search form-control" placeholder="Any Keyword"/>
                </div>
                <div class="col-md-5 col-lg-5 col-sm-5">
                    <button class="btn btn-{{Auth::user()->buttons}}" href="#" data-toggle="modal"
                            data-target="#export">
                        <b><span
                                    class="glyphicon glyphicon-export" style="font-size:Larger;"></span></b> Export
                    </button>
                    &nbsp;
                    <button data-toggle="modal" class="btn btn-{{Auth::user()->buttons}} " data-target="#addTabo"
                            style="margin-right: 16px;" area-label="add tabo tabo"><span
                                class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Item
                    </button>
                </div>

            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="panel panel-{{Auth::user()->panels}}">
                    <div class="panel-heading" style="height: 70px;">
                        <h4 class="pull-left" style="margin-top: 20px"> Recent List - Tabo Tabo</h4>
                        <div class="pull-right"> {!! $tabotabos->render() !!}</div>
                    </div>
                    <div class="panel-body" style="background-image: url('assets/img/.jpg')">
                        <div class="table table-responsive">
                            <table class="table table-hover col-lg-12 col-md-12 col-xs-12 results"
                                   xmlns="http://www.w3.org/1999/html">
                                <thead>
                                <th>Date of Purchase</th>
                                <th>Body#</th>
                                <th>Gender</th>
                                <th>Owner</th>
                                <th>Area</th>
                                <th>Color</th>
                                <th>Price</th>
                                <th>Gross</th>
                                <th>Expense</th>
                                <th>Income/Diff</th>
                                <th>Action</th>
                                <tr class="warning no-result">
                                    <td colspan="4"><i class="fa fa-warning"></i> No result</td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($tabotabos as $tabotabo)
                                    <tr>
                                        <td>{{$tabotabo['date_of_purchase']}}</td>
                                        <td>
                                            <a href="./viewTaboBr_{{$tabotabo['id']}}"
                                               class="btn btn-{{Auth::user()->buttons}}"
                                               data-toggle="tool-tip"
                                               data-placement="top"
                                               title="LW : {{$tabotabo['lw']}}   SW : {{$tabotabo['sw']}}   MW: {{$tabotabo['mw']}}
                                                       ">{{$tabotabo['id']}}</a>
                                        </td>
                                        <td>{{$tabotabo['sex']}}</td>
                                        <td>{{$tabotabo['owner']}}</td>
                                        <td>{{$tabotabo['area']}}</td>
                                        <td style="color:{{$tabotabo['color']}}">{{$tabotabo['color']}}</td>
                                        <td>₱{{number_format($tabotabo['price'],2)}}</td>
                                        <td>₱{{number_format($tabotabo['gross'],2)}}</td>
                                        <td>₱{{number_format($tabotabo['expense'],2)}}</td>

                                        <td>
                                          <b>  ₱{{number_format($tabotabo['income_deff'],2)}}</b>
                                        </td>

                                        <td><a href="#editTabo{{$tabotabo['id']}}" data-toggle="modal"
                                               data-target="#editTabo{{$tabotabo['id']}}"><span
                                                        class="glyphicon glyphicon-edit"></span></a> | <a
                                                    href="#remove{{$tabotabo['id']}}" data-toggle="modal"
                                                    data-target="#remove{{$tabotabo['id']}}"><span
                                                        class="glyphicon glyphicon-trash"></span></a></td>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- <label class = "label label-danger">Total Quantity :
                     Ordered Quantity :
                     Remaining Quantity : </label>
                     --->
                </div>
            </div>
        </div>

    </div>

    @foreach($tabotabos as $tabotabo)
        <div class="modal fade" id="editTabo{{$tabotabo['id']}}" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content"
                >
                    <div class="modal-header">
                        <button type="button" class="close"
                                data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Tabo</h4>
                    </div>
                    <div class="modal-body">
                        <form type="hidden" method="post"
                              action="./edit_tabo/{{$tabotabo['id']}}"
                              id="form1"/>
                        <input type="hidden" name="_token"
                               value="{{ csrf_token() }}"/>
                        <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                <label for="Date">Date Purchase:</label>
                                <input type='text'
                                       value="{{$tabotabo['date_of_purchase']}}"
                                       class="form-control"
                                       name="dateofpurchased" id='datetimepicker19'/>
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                <label for="bodyNum">Body #:</label>
                                <input type="text" value="{{$tabotabo['id']}}"
                                       placeholder="Body #"
                                       class="form-control" name="bodyNum">
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                <label for="owner">Owner:</label>
                                <input type="text" value="{{$tabotabo['owner']}}"
                                       placeholder="owner"
                                       class="form-control" name="owner">
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                <label for="area">Area:</label>
                                <input type="text" value="{{$tabotabo['area']}}"
                                       placeholder="area" class="form-control"
                                       name="area">
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                <label for="color">Color:</label>
                                <input type="text" value="{{$tabotabo['color']}}"
                                       placeholder="color"
                                       class="form-control" name="color">
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                <label for="gender">Gender</label>
                                <select name="gender" class="form-control">
                                    <option>{{$tabotabo['sex']}}</option>
                                    <option>Male</option>
                                    <option>Female</option>
                                </select>

                            </div>
                        </div>
                        <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                <label for="weight">LW:</label>
                                <input type="text" value="{{$tabotabo['lw']}}"
                                       placeholder="kg" class="form-control"
                                       name="lw">
                            </div>
                            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                <label for="sw">SW:</label>
                                <input type="text" value="{{$tabotabo['sw']}}"
                                       placeholder="kg" class="form-control"
                                       name="sw">
                            </div>
                            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                <label for="weight">MW:</label>
                                <input type="text" value="{{$tabotabo['mw']}}"
                                       placeholder="kg" class="form-control"
                                       name="mw">
                            </div>
                            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                <label for="perKilo">per Kilo:</label>
                                <input type="text"
                                       value="{{$tabotabo['price_per_kilo']}}"
                                       placeholder="per Kilo"
                                       class="form-control" name="perKilo">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-danger"><span
                                    class="glyphicon glyphicon-remove"></span>
                            Cancel
                        </button>
                        <button type="submit" class="btn btn-{{Auth::user()->buttons}}"><span
                                    class="glyphicon glyphicon-save"></span>
                            Save
                        </button>
                    </div>
                </div>
                </form>
            </div>

        </div>

        <div class="modal fade" id="remove{{$tabotabo['id']}}" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content"
                >
                    <div class="modal-header">
                        <button type="button" class="close"
                                data-dismiss="modal">&times;</button>
                        <h2 class="modal-title">Remove Transaction</h2>
                    </div>
                    <div class="modal-body">
                        <form type="hidden" method="post"
                              action="./delete_tabo/{{$tabotabo['id']}}" id="form1"/>
                        <input type="hidden" name="_token"
                               value="{{{ csrf_token() }}}"/>
                        <div class="container col-lg-12  col-md-12">
                            <h5> Are you sure you want to delete this transaction?</h5>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-danger"><span
                                    class="glyphicon glyphicon-remove"></span> Cancel
                        </button>
                        <button type="submit" class="btn btn-{{Auth::user()->buttons}}"><span
                                    class="glyphicon glyphicon-ok"></span>
                            Confirm
                        </button>
                    </div>
                </div>
                </form>
            </div>

        </div>
    @endforeach
    <div class="modal fade" id="export" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Export Records</h4>
                </div>
                <form type="hidden" method="post" action="./exportTabo" id="form1"/>
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                <div class="modal-body">
<!--                    <div class="form-group col-lg-4 col-md-6">
                        <label> Year </label>
                        <select class="form-control" name="year">
                            <option>2016</option>
                            <option>2017</option>
                            <option>2018</option>
                            <option>2019</option>
                            <option>2020</option>
                            <option>2021</option>
                            <option>2022</option>
                        </select>
                    </div>
                    <div class="form-group col-lg-4 col-md-6">
                        <label> Month </label>
                        <select class="form-control" name="month">
                            <option>January</option>
                            <option>Febuary</option>
                            <option>March</option>
                            <option>April</option>
                            <option>May</option>
                            <option>June</option>
                            <option>July</option>
                            <option>August</option>
                            <option>September</option>
                            <option>October</option>
                            <option>November</option>
                            <option>December</option>
                        </select>
                    </div>-->
                    <div class="form-group col-lg-6 col-md-6 col-sm-6">
                        <label for="">From :</label>
                        <input type='text' name="date_from" class="form-control"
                               id='datetimepicker1'/>
                    </div>
                    <div class="form-group col-lg-6 col-md-6 col-sm-6">
                        <label for="">To :</label>;
                        <input type='text' name="date_to" class="form-control"
                               id='datetimepicker2'/>
                    </div>
                    <div class="form-group col-lg-4 col-md-6">
                        <p><b>Note : </b> This will generate an excel file of Tabo Tabo</p>
                    </div>
                    <input type="hidden" name="all" value="0"/>
                    <label><input style="margin-left: 18px;" name="all" type="checkbox" value="1">Generate All</label>

                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-danger"><span
                                class="glyphicon glyphicon-remove"></span>
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-{{Auth::user()->buttons}}"><span
                                class="glyphicon glyphicon-export"></span>
                        Export
                    </button>
                </div>
            </div>
            </form>
        </div>

    </div>
    </div>



    <div class="modal fade" id="addTabo" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Tabo-Tabo</h4>
                </div>
                <div class="modal-body">
                    <form type="hidden" method="post" action="./addTabo" id="form1"/>
                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                    <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                            <label for="Date">Date Purchase:</label>
                            <input type='text' class="form-control" name="dateofpurchased" id='datetimepicker4'/>
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                            <label for="bodyNum">Body #:</label>
                            <input type="text" value="" placeholder="Body #"
                                   class="form-control" name="bodyNumber">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                            <label for="owner">Owner:</label>
                            <input type="text" value="" placeholder="owner"
                                   class="form-control" name="owner">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                            <label for="area">Area:</label>
                            <input type="text" value="" placeholder="area" class="form-control"
                                   name="area">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                            <label for="color">Color:</label>
                            <input type="text" value="" placeholder="color"
                                   class="form-control" name="color">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                            <label for="gender">Gender</label>
                            <select name="gender" class="form-control">
                                <option>Male</option>
                                <option>Female</option>
                            </select>

                        </div>
                    </div>
                    <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group col-lg-3 col-md-4 col-sm-3">
                            <label for="weight">LW:</label>
                            <input type="text" value="" placeholder="kg" class="form-control"
                                   name="lw">
                        </div>
                        <div class="form-group col-lg-3 col-md-4 col-sm-3">
                            <label for="sw">SW:</label>
                            <input type="text" value="" placeholder="kg" class="form-control"
                                   name="sw">
                        </div>
                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="weight">MW:</label>
                            <input type="text" value="" placeholder="kg" class="form-control"
                                   name="mw">
                        </div>
                        <div class="form-group col-lg-3 col-md-4 col-sm-3">
                            <label for="perKilo">per Kilo:</label>
                            <input type="text" value="" placeholder="per Kilo"
                                   class="form-control" name="perKilo">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-danger"><span
                                class="glyphicon glyphicon-remove"></span>
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-{{Auth::user()->buttons}}"><span
                                class="glyphicon glyphicon-plus"></span>
                        Save
                    </button>
                </div>
            </div>
            </form>
        </div>

    </div>

    <script>
        $(document).ready(function () {
            $(".search").keyup(function () {
                var searchTerm = $(".search").val();
                var listItem = $('.results tbody').children('tr').children('td');
                var searchSplit = searchTerm.replace(/ /g, "'):containsi('");
                $.extend($.expr[':'], {
                    'containsi': function (elem, i, match, array) {
                        return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
                    }
                });
                $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function (e) {
                    $(this).attr('visible', 'false');
                });
                $(".results tbody tr:containsi('" + searchSplit + "')").each(function (e) {
                    $(this).attr('visible', 'true');
                });
                var jobCount = $('.results tbody tr[visible="true"]').length;
                $('.counter').text(jobCount + ' item');
                if (jobCount == '0') {
                    $('.no-result').show();
                }
                else {
                    $('.no-result').hide();
                }
            });
        });
        var date1 = new Date();
        // document.getElementById("date1").value = date1.getDate() +"-0"+date1.getMonth()+"-"+date1.getFullYear();
        document.getElementById("arrow").innerHTML = "  Meatshop";
        document.getElementById("arrow1").innerHTML = "  Tabo Tabo";
        $(function () {
            $('#datetimepicker19').datepicker();
            $('#datetimepicker5').datepicker();
            $('#datetimepicker4').datepicker();
            $('#datetimepicker1').datepicker();
            $('#datetimepicker2').datepicker();
        });
    </script>
@endsection
