@extends('app')

@section('content')
    <style>
        #arrow1 {
            font-weight: bold;
        }

        .table td {
            text-align: left;
            padding: 2px;
        }

        .panel-body {
            padding: 5px;
        }

        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
            padding: 2px;
        }
        .table td {
          font-size : 16px;
        }
        .table th {
          font-size : 16px;
        }
    </style>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="panel panel-{{Auth::user()->panels}}">
                    <div class="panel-body">
                        <div style="text-align: center;font-size:17px;"><span class="glyphicon glyphicon-minus-sign"></span> Date :
                            <b>{{$cow['date']}}</b> &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp; <span
                                    class="glyphicon glyphicon-minus-sign"></span> Body# : <b>{{$cow['id']}}</b> &nbsp;&nbsp;
                            &nbsp; &nbsp;&nbsp; &nbsp;<span class="glyphicon glyphicon-minus-sign"></span> LW :
                            <b>{{$cow['lw']}}</b> Kls. &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; <span
                                    class="glyphicon glyphicon-minus-sign"></span> SW : <b>{{$cow['sw']}}</b> Kls.
                            &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp; <span class="glyphicon glyphicon-minus-sign"></span>
                            MW : <b>{{$cow['mw']}}</b> Kls.&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp; <span
                                    class="glyphicon glyphicon-minus-sign"></span> <b> QUARTER ANGUS</b> &nbsp;&nbsp;&nbsp;
                            &nbsp;Front:1. &nbsp;&nbsp; 2. &nbsp;&nbsp;&nbsp; &nbsp;Rear:1.&nbsp;&nbsp; 2.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="panel panel-{{Auth::user()->panels}}">
                    <div class="panel-heading">Base Parts</div>
                    <div class="panel-body" style="background-image: url('assets/img/1234567.jpg')">
                        <div class="table-responsive">
                            <table class="table table-hover col-lg-12 col-md-12 col-xs-12">
                                <thead>
                                <th>Article <br> Name</th>
                                <th>Quantity <br>(Kls.)</th>
                                </thead>
                                <tbody>
                                @foreach($cow['main_parts'] as $part)
                                    <tr>
                                        <td>{{$part['article']}}</td>
                                        <td>{{$part['quantity']}}</td>
                                    </tr>
                                @endforeach
                                <tr style="border-top:solid 2px;">
                                    <td><b>Total</b></td>
                                    <td><b>{{$cow['mw']}} Kls</b></td>
                                </tr>
                                <tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-10 col-md-10 col-sm-10">
                <div class="panel panel-{{Auth::user()->panels}}">
                    <div class="panel-heading"> Angus Inventory</div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered col-lg-12 col-md-12 col-xs-12">
                                <thead style="border-bottom:solid 2px;">
                                <th></th>
                                <th>Article <br>Name</th>
                                <th>Unit Price<br> (₱)</th>
                                <th>Total <br>Qty</th>
                                <th>Sales <br>Qty</th>
                                <th>Remain <br>Qty</th>
                                <th>Pack<br>()</th>
                                <th>Sales <br>(₱)</th>
                                <th>TOTAL <br>(₱)</th>
                                </thead>
                                <tbody>

                                <tr>
                                    <td><b>Choice Cuts</b></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                      <td></td>
                                </tr>
                                <?php $total1 = 0; ?>
                                @foreach($cow['article_resources'] as $list)
                                    @if($list['type']=='Choice Cuts')
                                        <tr>
                                            <td></td>
                                            <td>{{$list['article']['name']}} <label
                                                        class="label label-{{Auth::user()->labels}}">{{$list['id']}}</label>
                                            </td>

                                            <td>  @foreach($list['orderlist'] as $order)
                                                    {{number_format($order['unit_price'],2)}}
                                                    <br>
                                                @endforeach

                                            </td>
                                            <td>
                                                {{$list['quantity']}}


                                            </td>

                                            <td>  @foreach($list['orderlist'] as $order)
                                                    {{$order['qty']}}
                                                    <br>
                                                @endforeach

                                            </td>
                                            <td>{{$list['remain_qty']}}</td>
                                            <td>0</td>
                                            <td>  @foreach($list['orderlist'] as $order)
                                                    {{number_format($order['amount'],2)}}
                                                    <?php $total1 = $total1 + $order['amount'] ?>
                                                    <br>
                                                @endforeach

                                            </td>
                                            <td></td>
                                              <td></td>

                                        </tr>
                                    @endif
                                @endforeach
                                <tr style="border-top:solid 2px;">
                                    <td></td>
                                    <td></td>
                                    <td></td>

                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><b>₱{{number_format($total1,2)}}</b></td>

                                </tr>

                                <tr>
                                    <td><b>Special Cuts</b></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                      <td></td>
                                </tr>
                                <?php $total2 = 0; ?>
                                @foreach($cow['article_resources'] as $list)
                                    @if($list['type']=='Special Cuts')
                                        <tr>
                                            <td></td>
                                            <td>{{$list['article']['name']}} <label
                                                        class="label label-{{Auth::user()->labels}}">{{$list['id']}}</label>
                                            </td>

                                            <td>  @foreach($list['orderlist'] as $order)
                                                    {{number_format($order['unit_price'],2)}}
                                                    <br>
                                                @endforeach

                                            </td>
                                            <td>
                                                {{$list['quantity']}}


                                            </td>

                                            <td>  @foreach($list['orderlist'] as $order)
                                                    {{$order['qty']}}
                                                    <br>
                                                @endforeach

                                            </td>
                                            <td>{{$list['remain_qty']}}</td>
                                            <td>0</td>
                                            <td>  @foreach($list['orderlist'] as $order)
                                                    {{number_format($order['amount'],2)}}
                                                    <?php $total2 = $total2 + $order['amount'] ?>
                                                    <br>
                                                @endforeach

                                            </td>
                                              <td></td>
                                        </tr>
                                    @endif
                                @endforeach
                                <tr style="border-top:solid 2px;">
                                    <td></td>
                                    <td></td>
                                    <td></td>

                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><b>₱{{number_format($total2,2)}}</b></td>
                                </tr>


                                <tr>
                                    <td><b>Other Cuts</b></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                      <td></td>
                                </tr>
                                <?php $total3 = 0; ?>
                                @foreach($cow['article_resources'] as $list)
                                    @if($list['type']=='Other Cuts')
                                        <tr>
                                            <td></td>
                                            <td>{{$list['article']['name']}} <label
                                                        class="label label-{{Auth::user()->labels}}">{{$list['id']}}</label>
                                            </td>

                                            <td>  @foreach($list['orderlist'] as $order)
                                                    {{number_format($order['unit_price'],2)}}
                                                    <br>
                                                @endforeach

                                            </td>
                                            <td>
                                                {{$list['quantity']}}


                                            </td>

                                            <td>  @foreach($list['orderlist'] as $order)
                                                    {{$order['qty']}}
                                                    <br>
                                                @endforeach

                                            </td>
                                            <td>{{$list['remain_qty']}}</td>
                                            <td>0</td>
                                            <td>  @foreach($list['orderlist'] as $order)
                                                    {{number_format($order['amount'],2)}}
                                                    <?php $total3 = $total3 + $order['amount'] ?>
                                                    <br>
                                                @endforeach

                                            </td>
                                              <td></td>
                                        </tr>
                                    @endif
                                @endforeach
                                <tr style="border-top:solid 2px;">
                                    <td></td>
                                    <td></td>
                                    <td></td>

                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><b>₱{{number_format($total3,2)}}</b></td>

                                </tr>


                                <tr>
                                    <?php $total4 = $total1 + $total2 + $total3 ?>
                                    <td><b>TOTAL SALES</b></td>
                                    <td></td>
                                    <td></td>

                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td STYLE="border-bottom:solid 5px; "><b>₱{{number_format($total4,2)}}</b></td>

                                </tr>
                                <tbody>
                            </table>
                        </div>
                    </div>

                </div>
                <a href="./printCowReport{{$cow['id']}}" class="btn btn-{{Auth::user()->buttons}} btn-lg" ><span
                            class="glyphicon glyphicon-export"></span> Export to  excel</a>
            </div>
        </div>

        <script>
            document.getElementById("arrow").innerHTML = "  Meatshop";
            document.getElementById("arrow1").innerHTML = "  Body # {{$cow['id']}}";
        </script>
@endsection
