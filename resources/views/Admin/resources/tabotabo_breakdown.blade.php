@extends('app')

@section('content')
    <style>
        #arrow1 {
            font-weight: bold;
        }

        .table {
            width: 100%;
            margin-bottom: 2px;
        }

        .panel-body {
            padding: 8px;
        }

        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
            padding: 1px;
        }
        .table td {
          font-size : 17px;
        }
        .table th {
          font-size : 17px;
        }
    </style>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 col-lg-2 col-sm-2">
                <div class="panel panel-{{Auth::user()->panels}}">
                    <div class="panel-heading"><b>Basic Info</b></div>
                    <div class="panel-body" style="background-image: url('assets/img/.jpg')">
                       <pre> Date  : <b>{{$tabos['date_of_purchase']}}</b>
 Tattoo:
 Eartag no.:
 Body# : <b>{{$tabos['id']}}</b>
 LW    : <b>{{$tabos['lw']}}</b> Kgs.
 SW    : <b>{{$tabos['sw']}}</b> Kgs.
 MW    : <b>{{$tabos['mw']}}</b> Kgs.
 Via   : <b>{{$tabos['customer']}}</b>
</pre>
                    </div>
                </div>
            </div>
              <div class="col-md-10 col-sm-10 col-lg-10">
                <div class="panel panel-{{Auth::user()->panels}}">
                    <div class="panel-heading"><b>Income Statement Per Item</b></div>
                    <div class="panel-body" style="background-image: url('assets/img/.jpg')">
                        <div class="table table-responsive">
                            <table class="table table-bordered">
                                <thead style="padding: 0px 0px 0px 0px">
                                <th></th>
                                <th>Article <br>Expense</th>
                                <th>Quantity<br>(Kls)</th>
                                <th>Unit Price<br>(₱)</th>
                                <th>Sales<br>(₱)</th>
                                <th>Total<br>(₱)</th>
                                </thead>
                                <tbody>

                                <tr>
                                    <td><b>Sales : </b></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                @foreach($tabos['article_resources'] as $article)
                                    <tr>
                                        <td></td>
                                        <td>{{$article['article']['name']}} <label
                                                    class="label label-{{Auth::user()->labels}}">{{$article['id']}}</label>
                                        </td>
                                        <td>{{$article['sales_qty']}}</td>
                                        <td>{{number_format($article['unit_price'],2)}}</td>
                                        <td>{{number_format($article['total_sales'],2)}}</td>  <td></td>
                                    </tr>
                                @endforeach
                                <tr style="border-top:solid 2px;">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <center><b>₱{{number_format($tabos['gross'],2)}}</b></center>
                                    </td>

                                </tr>


                                <tr>
                                    <td><b>Expenses : </b></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>

                                @foreach($tabos['expenses'] as $expense)
                                    <tr>
                                        <td></td>
                                        <td>{{$expense['name']}}</td>
                                        <td></td>
                                        <td></td>
                                        <td>{{number_format($expense['amount'],2)}}</td>
                                        <td></td>
                                    </tr>
                                @endforeach

                                <tr style="border-top:solid 2px;">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td style="border-bottom: solid 2px;">
                                        <center><b>₱{{number_format($tabos['expense'],2)}}</b></center>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Gross Profit : </b></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td style="border-bottom:solid 5px;">
                                        <center><b>₱{{number_format($tabos['gross_profit'],2)}}</b></center>
                                    </td>
                                </tr>
                                </tbody>

                            </table>
                        </div>

                    </div>

                </div>

            </div>

        </div>
    </div>
    <div class="pull-right" style="padding: 0px 0px 0px 0px;">
        <a style="margin-right:12px;" href="./printTaboReport{{$tabos['id']}}"
           class="btn btn-{{Auth::user()->buttons}} btn-export btn-lg"><span
                    class="glyphicon glyphicon-export"></span> Export to excel</a>
        <button style="margin-right:12px;" data-toggle="modal" class="btn btn-{{Auth::user()->buttons}} btn-lg" data-target="#addArticle"
                 area-label="add tabo tabo"><span
                    class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
            Articles
        </button>
        <button style="margin-right:12px;"data-toggle="modal" class="btn btn-{{Auth::user()->buttons}} btn-lg" data-target="#addExpense"
                area-label="add tabo tabo"><span
                    class="glyphicon glyphicon-plus" aria-hidden="true"></span> Expenses
        </button>
        @if($tabos['status']==0)
            <button style="margin-right:12px;" class="btn btn-{{Auth::user()->buttons}} " data-toggle="modal" data-target="#addOrder"
                    style="margin-right: 16px;" aria-label="add order"><span
                        class="glyphicon glyphicon-shopping-cart"
                        aria-hidden="true"></span>
                Delivery
            </button>
        @endif
    </div>
    <div class="modal fade" id="addOrder" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">New Delivery</h4>
                </div>
                <div class="modal-body">
                    <form type="hidden" method="post" action="./addVendorOrder" id="form1"/>
                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                    <input type="hidden" name="id" value="{{$tabos['id']}}"/>

                    <div class="container col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                            <label for="Date">Date</label>
                            <input type='text' name="date1" class="form-control" id='datetimepicker14'/>
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                            <label for="DR">DR#:</label>
                            <input type="text" name="dr1" value="" placeholder=""
                                   class="form-control">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                            <label for="sold1">Delivered to</label>
                            <input name="customer2" value="" placeholder="" class="form-control">
                            </input>
                        </div>

                        <div class="container col-lg-4 col-md-4 col-sm-4">
                            <label for="dueDate">Due Date</label>
                            <input type='text' class="form-control" id='datetimepicker15' name="duedate1"/>
                        </div>

                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                            <label for="Qty2">Qty:</label>
                            <input type="text" placeholder="" value="{{$tabos['mw']}}" class="form-control"
                                   name="qty1">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                            <label for="unit2">Amount:</label>
                            <input type="text" value="" placeholder="" class="form-control" name="amount1">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-danger"><span
                                class="glyphicon glyphicon-remove"></span>
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-{{Auth::user()->buttons}}"><span
                                class="glyphicon glyphicon-save"></span> Confirm
                    </button>
                </div>
            </div>
            </form>
        </div>
    </div>
    <div class="modal fade" id="addExpense" role="dialog">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Expense (s)</h4>
                </div>
                <div class="modal-body">
                    <form type="hidden" method="post" action="./addExpense" id="form1">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <input type="hidden" name="id" value="{{$tabos['id']}}"/>
                        <div id="parentDIV" class="container col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                <label for="Date">Expense Name</label>
                                <input type='text' name="name0" placeholder="name here.."
                                       class="form-control"/>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                <label for="DR">Amount</label>
                                <input type="text" name="amount0" placeholder="amount here"
                                       class="form-control"/>
                            </div>

                        </div>
                        &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;<label onclick="AddData()"
                                                                    class="label label-info"
                                                                    style="font-size: medium"><span
                                    class="glyphicon glyphicon-plus"></span> Add Field
                        </label>
                        <div class="modal-footer">
                            <button data-dismiss="modal" class="btn btn-danger"><span
                                        class="glyphicon glyphicon-remove"></span>
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-{{Auth::user()->buttons}}"><span
                                        class="glyphicon glyphicon-save"></span>
                                Confirm
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addArticle" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Articles</h4>
                </div>
                <div class="modal-body">
                    <form type="hidden" method="post" action="./addArticles" id="form1"/>
                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                    <input type="hidden" name="id" value="{{$tabos['id']}}"/>
                    <div id="parentDIV1" class="container col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                            <label for="Date">Article</label>
                            <select type='text' name="" id="article" class="form-control"
                                    placeholder="articles">
                                @foreach($articles as $article)
                                    <option value="{{$article['name']}}">{{$article['name']}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="DR">Qty</label>
                            <input type="text" name="" id="qty" value="" placeholder="quantity"
                                   class="form-control">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                            <label for="DR">Unit_Price</label>
                            <input type="text" name="" id="unit" value="" placeholder="price"
                                   class="form-control">
                        </div>
                        <br>
                        <div class="form-group col-lg-1 col-md-1 col-sm-1" style="margin-top: 3px;">
                            <a onclick="AddData1()" class="btn btn-info"><span
                                        class="glyphicon glyphicon-plus"></span>
                            </a>
                        </div>

                    </div>

                    <div class="container col-lg-12">
                        <table class="table table-condensed" id="list1">
                            <thead id="tblHead">
                            <tr>
                                <th>Articles</th>
                                <th>Qty. (Kg)</th>
                                <th>Unit Price</th>
                                <th>Total Price (Php)</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>

                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-danger"><span
                                class="glyphicon glyphicon-remove"></span>
                        Cancel
                    </button>

                    <button type="submit" class="btn btn-{{Auth::user()->buttons}}"><span
                                class="glyphicon glyphicon-save"></span>
                        Confirm
                    </button>
                </div>
            </div>
            </form>
        </div>
    </div>
    <br><br>
    <script>
        document.getElementById("arrow").innerHTML = "  Meatshop";
        document.getElementById("arrow1").innerHTML = "  Body# {{$tabos['id']}}";
        $(function () {
            $('#datetimepicker15').datepicker();
            $('#datetimepicker14').datepicker();
        });

        var count = 0;
        function AddData() {
            count++;
            $('<div class="form-group col-lg-6"><input type="text"  placeholder="name here.." class="form-control" name="name' + count + '"/> </div> <div class="form-group col-lg-6"><input type="text"  placeholder="amount here" class="form-control" name="amount' + count + '"/> </div>').appendTo("#parentDIV");
        }
        var totalD = 0;
        var i = 0;
        function AddData1() {
            var rows = "";
            var totalP = "";
            var articles = document.getElementById("article").value;
            var qty = document.getElementById("qty").value;
            var unit = document.getElementById("unit").value;
            //var totalD = document.getElementById("totalD").innerHTML

            totalP = qty * unit;
            var TotalP = totalP.toFixed(2);
            rows += "<tr><td><input name=article" + i + " value='" + articles + "' style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;'></td><td><input name=qty" + i + " value='" + qty + "' style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;'></td><td><input name=unit_price" + i + " value='" + unit + "'style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;'></td><td><input name=total_due" + i + " value='" + TotalP + "' style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;'></td></tr>";
            $(rows).appendTo("#list1 tbody");
            i++;
        }

    </script>
@endsection
