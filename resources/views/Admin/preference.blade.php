@extends('app')

@section('content')
    <style>
        #arrow1 {
            font-weight: bold;
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="container col-lg-2">

                <div class="panel panel-{{Auth::user()->panels}}">
                    <div class="panel-heading"><h4>Options</h4></div>
                    <ul style="list-style: none;color: White;font-size: 18px;margin-right: 5px;">

                        @if(Auth::user()->userType == 'user')
                            <li><a href="./profile"><span class="glyphicon glyphicon-cog"></span> Profile</a></li>
                            <li><a href="./themes{{Auth::user()->id}}"><span
                                            class="glyphicon glyphicon-heart-empty"></span> Themes</a></li>
                        @elseif(Auth::user()->userType == 'Admin')
                            <li><a href="./profile"><span class="glyphicon glyphicon-cog"></span> Profile</a></li>
                            <li><a href="./articles"><span class="glyphicon glyphicon-list-alt"></span> Articles</a>
                            </li>
                            <li><a href="./themes{{Auth::user()->id}}"><span
                                            class="glyphicon glyphicon-heart-empty"></span> Themes</a></li>
                            <li><a href="./users"><span class="glyphicon glyphicon-user"></span> Users</a></li>
                        @endif
                    </ul>
                    <hr>
                </div>
            </div>
            @if(Auth::user()->userType == 'Admin')
                @if($action=='editprofile')
                    @include('profile')
                @elseif($action=='articles')
                    @include('articles')
                @elseif($action=='themes')
                    @include('themes')
                @elseif($action=='users')
                    @include('users')
                @endif
            @elseif(Auth::user()->userType == 'user')
                @if($action=='editprofile')
                    @include('profile')
                @elseif($action=='themes')
                    @include('themes')
                @endif
            @endif
        </div>
    </div>
    <script>
        @if(Auth::user()->userType == 'Admin')
         document.getElementById("arrow").innerHTML = "Meatshop";
        document.getElementById("arrow1").innerHTML = "Preferences";

        @elseif(Auth::user()->userType == 'user')
            document.getElementById("arrow1").innerHTML = "Preferences";
        @endif
    </script>
@endsection