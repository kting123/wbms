@extends('app')

@section('content')
    <style>
        #arrow {
            font-weight: bold;
        }
    </style>
    <div class="container-fluid" style="height: 50px;">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12 ">
                <div class="panel panel-{{Auth::user()->panels}} col-lg-3 col-md-3 col-sm-3 col-xs-3">

                    <div class="table table-responsive col-lg-12 col-md-12 col-xs-12">
                        <table class="table table-bordered col-lg-5 col-md-5 col-xs-5"
                               style=" overflow-x: auto;width: 250px;padding: 0px; border: 0px">
                            <thead>
                            <th>Articles</th>
                            </thead>
                            <tbody>
                            @foreach($articles as $data)
                                <tr>
                                    <td>{{$data['name']}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="panel panel-{{Auth::user()->panels}} col-lg-9 col-md-9 col-sm-9 col-xs-9  ">
                    <div class="table table-responsive col-lg-12 col-md-12 col-xs-12">
                        <table class="table table-bordered col-lg-8 col-md-8 col-xs-8"
                               style=" overflow-x: auto;width: 400px;padding: 0px; border: 0px">
                            <thead>
                            @foreach($date as $data)
                                <th>{{$data}}</th>
                            @endforeach
                            </thead>
                            <tbody>
                            @foreach($articles as $art)
                                <tr>
                                    @foreach($art['article_histories'] as $data)
                                        <td>{{$data['qty_remain']}}</td>
                                    @endforeach
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <script>
        document.getElementById("arrow").innerHTML = "Stock";
    </script>
@endsection