@extends('app')

@section('content')
    <style>
        #arrow1 {
            font-weight: bold;
        }

        .results tr[visible='false'],
        .no-result {
            display: none;
        }

        .results tr[visible='true'] {
            display: table-row;
        }

        .counter {
            padding: 8px;
            color: #ccc;

        ul.pagination {
            height: 10px;
        }

        .pagination {
            display: inline-block;
            padding-left: 0;
            margin: 0;
            border-radius: 4px;
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="pull-left col-md-3 col-lg-3 col-sm-3"  style="padding-left:0;">
                    <input class="search form-control" placeholder="Any Keyword"/>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-3">
                    <button class="btn btn-{{Auth::user()->buttons}}" href="#" data-toggle="modal"
                            data-target="#export">
                        <b><span
                                    class="glyphicon glyphicon-export" style="font-size:Larger;"></span></b> Export
                    </button>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                <div class="panel panel-{{Auth::user()->panels}}">
                    <div class="panel-heading" style="height: 80px;">
                        <h3 class="pull-left" style="margin-top: 20px"> Recent OR's</h3>
                        <label class="pull-right">{!! $order->render() !!}</label>
                    </div>
                    <div class="panel-body" style="background-image: url('assets/img/.jpg')">
                        <div class="table-responsive">
                            <div class="form-group col-lg-12">
                                <label class="radio-inline">
                                    <input type="radio" name="radBtn" id="full" value="1" checked> <b>
                                        Fully Paid</b>
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="radBtn" id="partial" value="0"> <b>Partially Paid</b>
                                </label>
                                &nbsp;&nbsp;
                            </div>
                            <table class="table table-hover col-lg-12 col-md-12 col-xs-12 results" id="full1">
                                <thead>
                                <th>Date of Order</th>
                                <th>OR#</th>
                                <th>Received From</th>
                                <th>Amount</th>
                                <th>Bank</th>
                                <th>Check#</th>
                                <th>Date Of Check</th>
                                <th>Type</th>
                                <th>Action</th>
                                <!-- <th>Action</th>-->
                                <tr class="warning no-result">
                                    <td colspan="4"><i class="fa fa-warning"></i> No result</td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($order as $data)
                                    <tr>
                                        <td>{{$data['date']}}</td>
                                        <td><a class = "btn btn-{{Auth::user()->buttons}} btn-sm"href="./order_or{{$data['id']}}" data-toggle="tool-tip"
                                               title="see details">{{$data['or']}}</a></td>
                                        <td>{{$data['customer']}}</td>
                                        <td>₱{{number_format($data['total_due'],2)}}</td>
                                        <td>{{$data['bank']}}</td>
                                        <td>{{$data['check']}}</td>
                                        <td>{{$data['date_of_check']}}</td>
                                        @if($data['type']=='delivery')
                                            <td><label class="label label-primary">{{$data['type']}}</label></td>

                                        @else
                                            <td><label class="label label-success">{{$data['type']}}</label></td>
                                        @endif
                                        <td><a href="#" data-toggle="modal" data-target="#edit{{$data['id']}}"><span
                                                        class="glyphicon glyphicon-edit"></span></a> |
                                            <a href="#" data-toggle="modal" data-target="#remove{{$data['id']}}"><span
                                                        class="glyphicon glyphicon-trash"></span></a></td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <table class="table table-hover col-lg-12 col-md-12 col-xs-12 results" id="partial1" hidden>
                                <thead>
                                <th>Date of Order</th>
                                <th>OR#</th>
                                <th>Received From</th>
                                <th>Amount</th>
                                <th>Bank</th>
                                <th>Check#</th>
                                <th>Date Of Check</th>
                                <th>Type</th>
                                <th>Action</th>
                                <!-- <th>Action</th>-->
                                <tr class="warning no-result">
                                    <td colspan="4"><i class="fa fa-warning"></i> No result</td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($paginations as $history)
                                    <tr>
                                        <td>{{$history['order']['date']}}</td>
                                        <td><label class = "label label-{{Auth::user()->labels}}">{{$history['or']}}</label></td>
                                        <td>{{$history['order']['customer']}}</td>
                                        <td>₱{{number_format($history['amount_paid'],2)}}</td>
                                        <td>{{$history['bank']}}</td>
                                        <td>{{$history['check']}}</td>
                                        <td>{{$history['date_of_check']}}</td>
                                        @if($history['order']['type']=='delivery')
                                            <td><label class="label label-primary">{{$history['order']['type']}}</label></td>

                                        @else
                                            <td><label class="label label-success">{{$history['order']['type']}}</label></td>
                                        @endif
                                        <td><a href="#" data-toggle="modal" data-target="#edit{{$history['order_id']}}"><span
                                                        class="glyphicon glyphicon-edit"></span></a> |
                                            <a href="#" data-toggle="modal" data-target="#remove{{$history['order_id']}}"><span
                                                        class="glyphicon glyphicon-trash"></span></a></td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <script>
                                $(document).ready(function () {
                                    $(function () {
                                        $('#datetimepicker4').datepicker();
                                        $('#datetimepicker5').datepicker();

                                    });
                                });
                            </script>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    @foreach($order as $data)

        <div class="modal fade" id="edit{{$data['id']}}" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h2 class="modal-title">Edit Transaction</h2>
                    </div>
                    <div class="modal-body">
                        <form type="hidden" method="post" action="./save_or/{{$data['id']}}" id="form1"/>
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                        <input type="hidden" name="month" value="{{$data['month']}}"/>
                        <input type="hidden" name="year" value="{{$data['year']}}"/>
                        <div class="container col-lg-12  col-md-12">

                            <div class="form-group col-lg-3  col-md-6">
                                <label for="Date">Date</label>

                                <input disabled type='text' class="form-control" id='datetimepicker4' name="date"
                                       value="{{$data['date']}}"/>

                            </div>
                            <div class="form-group col-lg-3 col-md-6">
                                <label for="DR">OR No.</label>
                                <input type="text" value="{{$data['or']}}" placeholder="" class="form-control"
                                       name="or">
                            </div>
                            <div class="form-group col-lg-3  col-md-6">
                                <label for="Deliver">Received From</label>
                                <input type="text" value="{{$data['customer']}}" placeholder="" class="form-control"
                                       name="customer" disabled>
                            </div>

                            <div class="form-group col-lg-3  col-md-6">
                                <label for="gender">Amount</label>
                                <input type="text" value="{{$data['total_due']}}" placeholder="" class="form-control"
                                       name="amount" disabled>
                            </div>
                            <div class="form-group col-lg-3  col-md-6">
                                <label for="Qty">Bank</label>
                                <input type="text" value="{{$data['bank']}}" placeholder="" class="form-control"
                                       name="bank">
                            </div>
                            <div class="form-group col-lg-3  col-md-6">
                                <label for="gender">Check No.</label>
                                <input type="text" value="{{$data['check']}}" placeholder="" class="form-control"
                                       name="checkno">
                            </div>
                            <div class="form-group col-lg-3  col-md-6">
                                <label for="Qty">Date Of Check</label>
                                <input type="text" id='datetimepicker5' value="{{$data['date_of_check']}}"
                                       class="form-control" name="dateofcheck">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-danger"><span
                                    class="glyphicon glyphicon-remove"></span> Cancel
                        </button>
                        <button type="submit" class="btn btn-{{Auth::user()->buttons}}"><span
                                    class="glyphicon glyphicon-save"></span>
                            Save
                        </button>
                    </div>
                </div>
                </form>
            </div>

        </div>
        <div class="modal fade" id="remove{{$data['id']}}" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h2 class="modal-title">Remove Transaction</h2>
                    </div>
                    <div class="modal-body">
                        <form type="hidden" method="post" action="./delete_or/{{$data['id']}}" id="form1"/>
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                        <div class="container col-lg-12  col-md-12">
                            <h5> Are you sure you want to delete this transaction?</h5>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-danger"><span
                                    class="glyphicon glyphicon-remove"></span> Cancel
                        </button>
                        <button type="submit" class="btn btn-{{Auth::user()->buttons}}"><span
                                    class="glyphicon glyphicon-ok"></span>
                            Confirm
                        </button>
                    </div>
                </div>
                </form>
            </div>

        </div>
    @endforeach
    <div class="modal fade" id="export" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Export OR's to Excel</h4>
                </div>
                <form type="hidden" method="post" action="./exportOR" id="form1"/>
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                <div class="modal-body">
                    <div class="form-group col-lg-4 col-md-6">
                        <label> Type </label>
                        <select class="form-control" name="type">
                            <option>Delivery</option>
                            <option>WalkedIn</option>
                            <option>WalkedIn&Delivery</option>
                        </select>
                    </div>
                    <div class="form-group col-lg-4 col-md-6">
                        <label> Year </label>
                        <select class="form-control" name="year">
                            <option>2016</option>
                            <option>2017</option>
                            <option>2018</option>
                            <option>2019</option>
                            <option>2020</option>
                            <option>2021</option>
                            <option>2022</option>
                        </select>
                    </div>
                    <div class="form-group col-lg-4 col-md-6">
                        <label> Month </label>
                        <select class="form-control" name="month">
                            <option>January</option>
                            <option>Febuary</option>
                            <option>March</option>
                            <option>April</option>
                            <option>May</option>
                            <option>June</option>
                            <option>July</option>
                            <option>August</option>
                            <option>September</option>
                            <option>October</option>
                            <option>November</option>
                            <option>December</option>
                        </select>
                    </div>
                    <input type="hidden" name="all" value="0"/>
                    <label><input style="margin-left: 18px;" name="all" type="checkbox" value="1">Generate All</label>
                    <p style="margin-left: 18px;"><b>Note : </b> This will generate an excel file of Official Receipts.
                    </p>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span>
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-{{Auth::user()->buttons}}"><span
                                class="glyphicon glyphicon-export"></span>
                        Export
                    </button>
                </div>
            </div>
            </form>
        </div>

    </div>
    <script>
        $(document).ready(function () {
            $(".search").keyup(function () {
                var searchTerm = $(".search").val();
                var listItem = $('.results tbody').children('tr').children('td');
                var searchSplit = searchTerm.replace(/ /g, "'):containsi('");
                $.extend($.expr[':'], {
                    'containsi': function (elem, i, match, array) {
                        return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
                    }
                });
                $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function (e) {
                    $(this).attr('visible', 'false');
                });
                $(".results tbody tr:containsi('" + searchSplit + "')").each(function (e) {
                    $(this).attr('visible', 'true');
                });
                var jobCount = $('.results tbody tr[visible="true"]').length;
                $('.counter').text(jobCount + ' item');
                if (jobCount == '0') {
                    $('.no-result').show();
                }
                else {
                    $('.no-result').hide();
                }
            });
        });
        var date1 = new Date();
        // document.getElementById("date1").value = date1.getDate() +"-0"+date1.getMonth()+"-"+date1.getFullYear();
        document.getElementById("arrow").innerHTML = "  Meatshop";
        document.getElementById("arrow1").innerHTML = "  Official Reciepts";

        $(document).ready(function () {
            $('input[type="radio"]').click(function () {
                if ($(this).attr('id') == 'full') {
                    $('#full1').show();
                    $('#partial1').hide();
                }
                else {
                    $('#full1').hide();
                    $('#partial1').show();
                   }
            });
        });
    </script>
@endsection
