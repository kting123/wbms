@extends('app')

@section('content')
    <style>
        #arrow1 {
            font-weight: bold;
        }

        .table td {
          font-size : 16px;
        }
        .table th {
          font-size : 16px;
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="panel panel-{{Auth::user()->panels}}">
                    <div class="panel-heading">Transaction Breakdown</div>
                    <div class="panel-body" style="background-image: url('assets/img/1234567.jpg')">
                        <label class="label label-{{Auth::user()->labels}}" style="font-size:medium;"> Order List</label>
                        </br>
                        <div class="container col-lg-12 col-md-12 col-xs-12">
                            <div class="table-responsive">
                                <table class="table table-hover col-lg-12 col-md-12 col-xs-12">
                                    <thead>
                                    <th>Article</th>
                                    <th>Qty</th>
                                    <th>Unit Price</th>
                                    <th>Amount</th>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $list)
                                        <tr>
                                            <td>{{$list['article']['name']}} (<a href = "#" data-toggle="tooltip"
                                                                                title="{{$list['from_article']}} ">{{$list['article__resource_id']}} </a>)
                                                </td>
                                            <td>{{$list['qty']}}</td>
                                            <td>₱{{number_format($list['unit_price'],2)}}</td>
                                            <td>₱{{number_format($list['amount'],2)}}</td>
                                        </tr>
                                    @endforeach
                                    <tr>

                                        <td></td>
                                        <td></td>
                                        <td><strong>Total Dues</strong></td>
                                        <td><u><strong>₱{{number_format($list['order']['total_due'],2)}}</strong></u></td>
                                    </tr>
                                    <tbody>
                                </table>

                            </div>
                            <div class="table-responsive">
                                <td><h5><b>History of Payments</b></h5></td>
                                <table class="table table-bordered col-lg-12 col-md-12 col-xs-12">
                                    <thead style="border-bottom:solid 2px;">
                                    <th></th>
                                    <th>Date of Payment</th>
                                    <th>Amount (Php)</th>
                                    <th>OR #</th>
                                    <th>Bank Name</th>
                                    <th>Check No.</th>
                                    <th>Balance (Php)</th>
                                    </thead>
                                    <tbody>
                                    @foreach($history as $histories)
                                        <tr>
                                            <td></td>
                                            <td>{{$histories['date_of_check']}}</td>
                                            <td>{{number_format($histories->amount_paid,2)}}</td>
                                            <td>{{$histories->or}}</td>
                                            <td>{{$histories->bank}}</td>
                                            <td>{{$histories->check}}</td>
                                            <td>{{number_format($histories->balance,2)}}</td>
                                        </tr>
                                    @endforeach
                                    <tr style="border-top:solid 2px;">
                                        <td style="background-color: #c6cad5;"><h5><b>Current Balance</b></h5></td>
                                        <td style="background-color: #c6cad5;"></td>
                                        <td style="background-color: #c6cad5;"></td>
                                        <td style="background-color: #c6cad5;"></td>
                                        <td style="background-color: #c6cad5;"></td>
                                        <td style="background-color: #c6cad5;"></td>
                                        <td style="background-color: #c6cad5;"><b><br>{{number_format($list['order']['balance'],2)}}</b></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="pull-right">
                            <label class="label label-{{Auth::user()->labels}}" style="font-size:medium;">Payment
                                Posted {{$list['order']['date_of_check']}}</label>
                        </div>

                    </div>
                    <pre style="text-align: center">Customer : <b>{{$list['order']['customer']}}</b>  |  Date : <b>{{$list['order']['date']}}</b>  |  DR : <b>{{$list['order']['dr']}}</b>  |  OR : <b>{{$list['order']['or']}}</b>  |  Status : <b>{{$list['order']['status']}}</b>  |  Payment Posted : <b>{{$list['order']['date_of_check']}}</b></pre>
                </div>
            </div>

        </div>
    </div>
    </div>
    </div>
    </div>
    </div>

    <script>
        document.getElementById("arrow").innerHTML = "  Meatshop";
        document.getElementById("arrow1").innerHTML = "  OR # {{$list['order']['or']}}";
    </script>
@endsection
