@extends('app')

@section('content')
    <style>
        #arrow1 {
            font-weight: bold;
        }

        .results tr[visible='false'],
        .no-result {
            display: none;
        }

        .results tr[visible='true'] {
            display: table-row;
        }

        .counter {
            padding: 8px;
            color: #ccc;
        }
    
    </style>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="pull-left col-md-3 col-lg-3 col-sm-3"  style="padding-left:0;">
                    <input class="search form-control" placeholder="Any Keyword"/>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-3">
                    <button class="btn btn-{{Auth::user()->buttons}}" href="#" data-toggle="modal"
                            data-target="#export">
                        <b><span
                                    class="glyphicon glyphicon-export" style="font-size:Larger;"></span></b> Export
                    </button>
                </div>
            </div>

        </div>
        <br>
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="panel panel-{{Auth::user()->panels}}">
                    <div class="panel-heading" style="height: 80px;">
                        <h3 class="pull-left" style="margin-top: 20px"> Recent DR's</h3>
                        <label class="pull-right">{!! $order->render() !!}</label>
                    </div>
                    <div class="panel-body " style="background-image: url('assets/img/.jpg')">
                        <div class="table-responsive">
                            <table class="table table-hover col-lg-12 col-md-12 col-xs-12 results">
                                <thead>
                                <th>Date</th>
                                <th>DR#</th>
                                <th>Delivered to</th>
                                <th>Amount</th>
                                <th>Status</th>
                                <th>Due Date</th>
                                <th>Action</th>
                                <tr class="warning no-result">
                                    <td colspan="4"><i class="fa fa-warning"></i> No result</td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($order as $data)
                                    <tr>
                                        @if($data['status']=="cancelled")
                                            <td><strike>{{$data['date']}}<strike></td>
                                            <td><a href="./order_dr{{$data['id']}}" data-toggle="tool-tip"
                                                   title="see details"><strike>{{$data['dr']}}</strike></a>
                                            </td>
                                            <td><strike>{{$data['customer']}}<strike></td>
                                            <td><strike>₱{{number_format($data['total_due'], 2)}}</strike></td>
                                            <td><strike><label
                                                            class="label label-danger">{{$data['status']}}</label></strike>
                                            </td>
                                            <td><strike>{{$data['due_date']}}</strike></td>
                                        @else
                                            <td>{{$data['date']}}</td>
                                            <td><a  class="btn btn-{{Auth::user()->buttons}} btn-xs"href="./order_dr{{$data['id']}}" data-toggle="tool-tip"
                                                   title="see details">{{$data['dr']}}</a>
                                            </td>
                                            <td>{{$data['customer']}}</td>
                                            <td>₱{{number_format($data['total_due'], 2)}}</td>
                                            <?php
                                            $due = (int)strtotime($data['due_date']);
                                            $due_warning = $due - 432000;
                                            ?>
                                            @if($data['status']=="paid")
                                                <td><label class="label label-success" style="font-size:medium;">{{$data['status']}}</label></td>
                                                <td><label class="label label-success" style="font-size:medium;">{{$data['due_date']}}</label>
                                                </td>
                                            @else
                                                @if(($data['status']=="pending" || $data['status']=="partial") && $due <= $now)
                                                    <td><label class="label label-warning" style="font-size:medium;">{{$data['status']}}</label>
                                                    </td>
                                                    <td><label class="label label-danger" style="font-size:medium;">{{$data['due_date']}}</label>
                                                    </td>
                                                @elseif(($data['status']=="pending" || $data['status']=="partial") && $due_warning <=$now )
                                                    <td><label class="label label-warning" style="font-size:medium;">{{$data['status']}}</label>
                                                    </td>
                                                    <td><label class="label label-warning" style="font-size:medium;">{{$data['due_date']}}</label>
                                                    </td>
                                                @else
                                                    <td><label class="label label-warning" style="font-size:medium;">{{$data['status']}}</label>
                                                    </td>
                                                    <td><label class="label label-info" style="font-size:medium;">{{$data['due_date']}}</label>
                                                    </td>
                                                @endif
                                            @endif
                                        @endif
                                        <td><a href="#" data-toggle="modal"
                                               data-target="#edit{{$data['id']}}"><span
                                                        class="glyphicon glyphicon-edit"></span></a> |
                                            <a href="#"
                                               data-toggle="modal"
                                               data-target="#remove{{$data['id']}}"><span
                                                        class="glyphicon glyphicon-trash"></span></a>
                                        </td>
                                    </tr>

                                @endforeach
                                </tbody>
                            </table>

                            <script>
                                $(document).ready(function () {
                                    $(function () {
                                        $('#datetimepicker4').datepicker();
                                        $('#datetimepicker5').datepicker();
                                    });
                                });
                            </script>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    @foreach($order as $data)
        <div class="modal fade" id="edit{{$data['id']}}" role="dialog">
            <div class="modal-dialog modal-lg">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close"
                                data-dismiss="modal">&times;</button>
                        <h2 class="modal-title">Edit Transaction</h2>
                    </div>
                    <div class="modal-body">
                        <form type="hidden" method="post" action="./save_dr/{{$data['id']}}"
                              id="form1"/>
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                        <div class="container col-lg-12  col-md-12">

                            <div class="form-group col-lg-3  col-md-6">
                                <label for="Date">Date</label>

                                <input type='text' class="form-control" id='datetimepicker4'
                                       name="date"
                                       value="{{$data['date']}}"/>

                            </div>
                            <div class="form-group col-lg-3 col-md-6">
                                <label for="DR">DR No.</label>
                                <input type="text" value="{{$data['dr']}}" placeholder=""
                                       class="form-control"
                                       name="dr">
                            </div>
                            <div class="form-group col-lg-3  col-md-6">
                                <label for="Deliver">Delivered to</label>
                                <input type="text" value="{{$data['customer']}}" placeholder=""
                                       class="form-control"
                                       disabled name="customer">
                            </div>

                            <div class="form-group col-lg-3  col-md-6">
                                <label for="gender">Amount</label>
                                <input type="text" value="{{$data['total_due']}}" placeholder=""
                                       class="form-control"
                                       name="amount">
                            </div>
                            <div class="form-group col-lg-3  col-md-6">
                                <label for="gender">Status</label>
                                <select class="form-control" name="status">
                                    <option>{{$data['status']}}</option>
                                    <option>pending</option>
                                    <option>paid</option>
                                    <option>cancelled</option>
                                </select>

                            </div>
                            <div class="form-group col-lg-3  col-md-6">
                                <label for="Qty">Due Date</label>
                                <input type="text" id='datetimepicker5'
                                       value="{{$data['due_date']}}"
                                       class="form-control" name="due_date">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-danger"><span
                                    class="glyphicon glyphicon-remove"></span> Cancel
                        </button>
                        <button type="submit" class="btn btn-success"><span
                                    class="glyphicon glyphicon-save"></span>
                            Save
                        </button>
                    </div>
                </div>
                </form>
            </div>

        </div>
        <div class="modal fade" id="remove{{$data['id']}}" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close"
                                data-dismiss="modal">&times;</button>
                        <h2 class="modal-title">Remove Transaction</h2>
                    </div>
                    <div class="modal-body">
                        <form type="hidden" method="post" action="./delete_dr/{{$data['id']}}"
                              id="form1"/>
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                        <div class="container col-lg-12  col-md-12">
                            <h5> Are you sure you want to delete this transaction?</h5>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-danger"><span
                                    class="glyphicon glyphicon-remove"></span> Cancel
                        </button>
                        <button type="submit" class="btn btn-success"><span
                                    class="glyphicon glyphicon-ok"></span>
                            Confirm
                        </button>
                    </div>
                </div>
                </form>
            </div>

        </div>
    @endforeach

    <div class="modal fade" id="export" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Export DR's</h4>
                </div>
                <div class="modal-body">
                    <form type="hidden" method="post" action="./exportDR" id="form1"/>
                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                    <div class="modal-body">
                        <div class="form-group col-lg-4 col-md-6">
                            <label> Year </label>
                            <select class="form-control" name="year">
                                <option>2016</option>
                                <option>2017</option>
                                <option>2018</option>
                                <option>2019</option>
                                <option>2020</option>
                                <option>2021</option>
                                <option>2022</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-4 col-md-6">
                            <label> Month </label>
                            <select class="form-control" name="month">
                                <option>January</option>
                                <option>Febuary</option>
                                <option>March</option>
                                <option>April</option>
                                <option>May</option>
                                <option>June</option>
                                <option>July</option>
                                <option>August</option>
                                <option>September</option>
                                <option>October</option>
                                <option>November</option>
                                <option>December</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-4 col-md-6">
                            <p><b>Note : </b> This will generate an excel file of Delivery Receipts.</p>
                        </div>
                        <input type="hidden" name="all" value="0"/>
                        <label><input style="margin-left: 18px;" name="all" type="checkbox" value="1">Generate
                            All</label>

                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-danger"><span
                                    class="glyphicon glyphicon-remove"></span>
                            Cancel
                        </button>
                        <button type="submit" class="btn btn-{{Auth::user()->buttons}}"><span
                                    class="glyphicon glyphicon-export"></span>
                            Export
                        </button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $(".search").keyup(function () {
                var searchTerm = $(".search").val();
                var listItem = $('.results tbody').children('tr').children('td');
                var searchSplit = searchTerm.replace(/ /g, "'):containsi('");
                $.extend($.expr[':'], {
                    'containsi': function (elem, i, match, array) {
                        return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
                    }
                });
                $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function (e) {
                    $(this).attr('visible', 'false');
                });
                $(".results tbody tr:containsi('" + searchSplit + "')").each(function (e) {
                    $(this).attr('visible', 'true');
                });
                var jobCount = $('.results tbody tr[visible="true"]').length;
                $('.counter').text(jobCount + ' item');
                if (jobCount == '0') {
                    $('.no-result').show();
                }
                else {
                    $('.no-result').hide();
                }
            });
        });
        var date1 = new Date();
        // document.getElementById("date1").value = date1.getDate() +"-0"+date1.getMonth()+"-"+date1.getFullYear();
        document.getElementById("arrow").innerHTML = "  Meatshop";
        document.getElementById("arrow1").innerHTML = "  Delivery Reciepts";

    </script>
@endsection
