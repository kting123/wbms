@extends('app')

@section('content')
    <style>
        #arrow1 {
            font-weight: bold;
        }
        .table td {
          font-size : 16px;
        }
        .table th {
          font-size : 16px;
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="panel panel-{{Auth::user()->panels}}">
                    <div class="panel-heading">Transaction Breakdown</div>
                    <div class="panel-body">
                        <label class="label label-{{Auth::user()->labels}}" style="font-size:medium;"> Order
                            List</label>

                        </br>

                        <div class="container col-lg-12 col-md-12 col-xs-12">
                            <div class="table-responsive">
                                <table class="table table-hover col-lg-12 col-md-12 col-xs-12">
                                    <thead>
                                    <th>Article</th>
                                    <th>Qty</th>
                                    <th>Unit Price</th>
                                    <th>Amount</th>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $list)
                                        <tr>
                                            <td>{{$list['article']['name']}} (<a href = "#" data-toggle="modal" data-target = "#{{$list['id']}}">{{$list['article__resource_id']}}) </a></td>
                                            <td>{{$list['qty']}}</td>
                                            <td>₱{{number_format($list['unit_price'],2)}}</td>
                                            <td>₱{{number_format($list['amount'],2)}}</td>
                                        </tr>

                                    @endforeach
                                    <tr>

                                        <td></td>
                                        <td></td>
                                        <td><strong>Total Dues</strong></td>
                                        <td><u><strong>₱{{number_format($list['order']['total_due'],2)}}</strong></u>
                                        </td>
                                    </tr>
                                    <tbody>
                                </table>

                            </div>

                            @if($list['order']['status']=="paid")
                                <div class="pull-right">
                                    <label class="label label-{{Auth::user()->labels}}" style="font-size:medium;">Payment
                                        Posted
                                        : {{$list['order']['date_of_check']}}</label>
                                </div>
                            @elseif($list['order']['status']=="cancelled")
                                <div class="pull-right">
                                    <label class="label label-danger" style="font-size:medium;">Transaction
                                        Cancelled</label>
                                </div>
                            @else
                                <div style="padding-top: 20px;"><a>
                                        <button class="btn btn-{{Auth::user()->buttons}} btn-lg" data-toggle="modal"
                                                data-target="#pay"><span
                                                    class="glyphicon glyphicon-credit-card"></span> Post
                                            Payment

                                        </button>
                                    </a></div>
                        </div>
                        @endif

                    </div>

                    <pre style="text-align: center">Customer : <b>{{$list['order']['customer']}}</b>  |  Date : <b>{{$list['order']['date']}}</b>  |  DR : <b>{{$list['order']['dr']}}</b>  |  OR : <b>{{$list['order']['or']}}</b>  |  Status : <b>{{$list['order']['status']}}</b> </pre>
                </div>

            </div>
        </div>

    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    <div class="modal fade" id="pay" role="dialog">
        <div class="modal-dialog modal-md">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Post Payment</h4>
                </div>
                <div class="modal-body">
                    <form type="hidden" method="post" action="./postpayment/{{$list['order']['id']}}" id="form1"/>
                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                    <div class="table-responsive">
                        <td><h5><b>History of Payments</b></h5></td>
                        <table class="table table-bordered col-lg-12 col-md-12 col-xs-12">
                            <thead style="border-bottom:solid 2px;">
                            <th></th>
                            <th>Date of Payment</th>
                            <th>Amount (Php)</th>
                            <th>OR #</th>
                            <th>Bank Name</th>
                            <th>Check No.</th>
                            <th>Balance (Php)</th>
                            </thead>
                            <tbody>
                            @foreach($history as $histories)
                                <tr>
                                    <td></td>
                                    <td>{{$histories['date_of_check']}}</td>
                                    <td>{{number_format($histories->amount_paid,2)}}</td>
                                    <td>{{$histories->or}}</td>
                                    <td>{{$histories->bank}}</td>
                                    <td>{{$histories->check}}</td>
                                    <td>{{number_format($histories->balance,2)}}</td>
                                </tr>
                            @endforeach
                            <tr style="border-top:solid 2px;">
                                <td style="background-color: #c6cad5;"><h5><b>Current Balance</b></h5></td>
                                <td style="background-color: #c6cad5;"></td>
                                <td style="background-color: #c6cad5;"></td>
                                <td style="background-color: #c6cad5;"></td>
                                <td style="background-color: #c6cad5;"></td>
                                <td style="background-color: #c6cad5;"></td>
                                <td style="background-color: #c6cad5;"><b><br>{{number_format($list['order']['balance'],2)}}</b></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-12">
                        <label class="checkbox-inline" style="margin-left: 10px;"> <input type="checkbox" id="cheque"> Pay in Cheque</input>
                        </label>
                    </div>
                    <div class="form-group col-lg-12 col-md-12">
                        <div class="col-md-4 col-lg-4 col-xs-12">
                            <label>Amount</label>
                            <input placeholder="" value="{{$list['order']['balance']}}" name="amount"
                                   class="form-control">
                            </input>
                        </div>
                        <div class="col-md-4 col-lg-4col-xs-12">
                            <label>OR No.</label>
                            <input placeholder="or" name="or" class="form-control">
                            </input>
                        </div>
                        <div class="col-md-4 col-lg-4 col-xs-12">
                            <label>Date Of Payment</label>
                            <input placeholder="" name="dateofcheck" id="date2" class="form-control">
                            </input>
                        </div>
                        <div class="col-md-4 col-lg-4 col-xs-12" id="bank" hidden>
                            <label>Bank</label>
                            <input placeholder="Bank Name" name="bank" id="banks" class="form-control" disabled>
                            </input>
                        </div>
                        <div class="col-md-4 col-lg-4 col-xs-12" id="checkNum" hidden>
                            <label>Check No.</label>
                            <input placeholder="check no" name="checkno" id="check" class="form-control" disabled>
                            </input>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span>
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-{{Auth::user()->buttons}}"><span
                                class="glyphicon glyphicon-ok"></span> Post
                        Payment
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>

@foreach($data as $list)

    <div class="modal fade" id="{{$list['id']}}" role="dialog">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Item Details</h4>
                </div>
                <div class="modal-body">
                  <p>
                    This item is coming from <b>{{$list['from_article']}}</b> which listed as part of this ref_ID : <b> {{$list['article__resource_id']}}</b>
                  </p>
            </div>
        </div>
    </div>
    </div>
@endforeach


    <script>
        $(document).ready(function () {
            $(function () {
                $('#date2').datepicker("setDate", '1d');
            });
        });
        document.getElementById("arrow").innerHTML = "  Meatshop";
        document.getElementById("arrow1").innerHTML = " DR# {{$list['order']['dr']}}";

        $(document).ready(function () {
            $('#cheque').click(function () {
                if ($(this).is(":checked")) {
                    // check = 1;
                    $('#bank').show();
                    $('#checkNum').show();
                    document.getElementById("banks").disabled = false;
                    document.getElementById("check").disabled = false;

                }
                else {
                    $('#bank').hide();
                    $('#checkNum').hide();
                    document.getElementById("banks").disabled = true;
                    document.getElementById("check").disabled = true;
                    //  check = 0;
                }
            });
        });

    </script>
@endsection
