<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>WB's Monitoring System</title>

    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="./assets/css/bootstrap-datepicker.min.css">
    <!-- Fonts -->

    <link rel="stylesheet" href="./assets/css/bootstrap.min.css">
    <script src="./assets/js/jquery.min.js"></script>

    <![endif]-->
</head>
<style>
    .panel {
        opacity: {{Auth::user()->opacity}};
    }

    .modal-footer {
        margin-top: 5px;
        padding: 15px 15px 15px;
        text-align: right;
        border-top: 1px solid #e5e5e5;
    }
    .table td {
      font-size : 17px;
    }
    .table th {
      font-size : 17px;
    }
</style>
@if(Auth::user()->backimage == '1')
    <body style="background: url(assets/img/{{"b".Auth::user()->id}}.jpg) no-repeat center center fixed;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;">
    @elseif(Auth::user()->backimage == '0')
        <body style="background-color: {{Auth::user()->bc}};">
        @endif
        <nav class="navbar navbar-{{Auth::user()->bi}} navbar-fixed-top">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="navbar-header ">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle Navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a id="home" class="navbar-brand" href="./home"> Home</a>
                            <a id="arrow" class="navbar-brand" href="./meatshop"></a>
                            <a id="arrow1" class="navbar-brand" href=""></a>
                            <a id="arrow2" class="navbar-brand" href=""></a>
                            <a id="arrow3" class="navbar-brand" href="#"></a>
                            <a id="arrow4" class="navbar-brand" href="#"></a>
                            <a id="arrow5" class="navbar-brand" href="#"></a>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <!--
                    <ul class="nav navbar-nav">
                            <li><a href="{{ url('/') }}">Home</a></li>
                    </ul>
                            --->
                            <ul class="nav navbar-nav navbar-right">
                                @if (Auth::guest())

                                    <li><a href="{{ url('/auth/login') }}">Login</a></li>
                                @else
                                    <li><a href="#" role="button"
                                           aria-expanded="false"><span class="glyphicon glyphicon-log-in"></span>
                                            Logged as {{Auth::user()->userType}}</a></li>
                                    <li><a href="./showStock" role="button"
                                           aria-expanded="false"><span class="glyphicon glyphicon-star"></span>
                                            Stock Status </a></li>

                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                           aria-expanded="false"><span
                                                    class="glyphicon glyphicon-user"></span> {{ Auth::user()->name }}
                                            <span
                                                    class="caret"></span></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="{{ url('/preferences') }}"> <span
                                                            class="glyphicon glyphicon-cog"></span> Preferences</a>
                                            </li>
                                            @if(Auth::user()->userType == 'Admin' || Auth::user()->admin == '1')
                                                <li><a href="{{ url('/change_user') }}"> <span
                                                                class="glyphicon glyphicon-user"></span>Switch User Type</a>
                                                </li>
                                            @endif
                                            <li><a href="{{ url('/auth/logout') }}"> <span
                                                            class="glyphicon glyphicon-log-out"></span> Logout</a></li>

                                        </ul>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <div style="text-align: center; width: auto; color: white">
        </div>
        <br><br><br>@include('flash::message')<br>
        @yield('content')


        <!-- Scripts -->
        <script>
            $('.alert').delay(3000).slideUp(300);
        </script>
        <script src="./assets/js/bootstrap.min.js"></script>
        <script src="./assets/js/jquery-ui.js"></script>
        <script src="./assets/js/bootstrap-datepicker.min.js"></script>
        </body>
</html>
