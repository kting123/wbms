<div class="col-lg-10">
    <div class="panel panel-{{Auth::user()->panels}}" style=height:500px;">
        <div class="panel-heading"><h4>Manage Themes</h4></div>
        <form type="hidden" method="post" action="./editThemes" id="form1" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            <br>
            <label for="" class="label label-{{Auth::user()->labels}} pull-left" style="font-size: medium">Panel
                Headers /  Buttons/
                 /Labels  </label>
            <br>
            <br>
            <div class="container col-lg-12 col-md-12 col-sm-12">
                <div class="form-group col-lg-3 col-md-3 col-sm-3">
                    <label for="sold1">Panels</label>
                    <select name="panel" value="" placeholder="" class="form-control">
                        <option value="{{Auth::user()->panels}}">{{Auth::user()->panels}}</option>
                        <option value="primary" style="color: #337ab7;">Primary</option>
                        <option value="success" style="color: #5cb85c;">Success</option>
                        <option value="info" style="color: #5bc0de;">Info</option>
                        <option value="default" style="color: #ccc;">Default</option>
                        <option value="warning" style="color: #f0ad4e;">Warning</option>
                        <option value="danger" style="color: #c9302c;">Danger</option>
                    </select>
                </div>
                <div class="form-group col-lg-3 col-md-3 col-sm-3">
                    <label for="sold1">Buttons</label>
                    <select name="button" value="" placeholder="" class="form-control">
                        <option value="{{Auth::user()->buttons}}">{{Auth::user()->buttons}}</option>
                        <option value="primary" style="color: #337ab7;">Primary</option>
                        <option value="success" style="color: #5cb85c;">Success</option>
                        <option value="info" style="color: #5bc0de;">Info</option>
                        <option value="default" style="color: #ccc;">Default</option>
                        <option value="warning" style="color: #f0ad4e;">Warning</option>
                        <option value="danger" style="color: #c9302c;">Danger</option>
                    </select>
                </div>
                <div class="form-group col-lg-3 col-md-3 col-sm-3">
                    <label for="sold1">Labels</label>
                    <select name="label" value="" placeholder="" class="form-control">
                        <option value="{{Auth::user()->labels}}">{{Auth::user()->labels}}</option>
                        <option value="primary" style="color: #337ab7;">Primary</option>
                        <option value="success" style="color: #5cb85c;">Success</option>
                        <option value="info" style="color: #5bc0de;">Info</option>
                        <option value="default" style="color: #ccc;">Default</option>
                        <option value="warning" style="color: #f0ad4e;">Warning</option>
                        <option value="danger" style="color: #c9302c;">Danger</option>
                    </select>
                </div>
                <div class="form-group col-lg-3 col-md-3 col-sm-3">
                    <label for="sold1">Nav Bar</label>
                    <select name="bi" value="" placeholder="" class="form-control">
                        <option value="{{Auth::user()->bi}}">{{Auth::user()->bi}}</option>
                        <option value="inverse" style="color: black;">Inverse(Dark)</option>
                        <option value="default" style="color: #ccc;">Default(White)</option>

                    </select>
                </div>
            </div>
              <br>    <br>
            <label for="" class="label label-{{Auth::user()->labels}} pull-left" style="font-size: medium">
                Background color/Image & transparency</label>
            <br>
            <div class="container col-lg-12 col-md-12 col-sm-12">
                <div class="form-group col-lg-3 col-md-3 col-sm-3" style="margin-left:10px; ">
                    @if(Auth::user()->backimage =='0')
                        <label> <input type="radio" name="pref" value="0" checked/>Background Color</label>
                        <input type='color' class="form-control" value="{{Auth::user()->bc}}" name="bc"/>
                </div>

                <div class="form-group col-lg-2 col-md-2 col-sm-2">
                    <label>Transparency</label>
                    <input type='text' data-toggle="tool-tip" title="0 - 1 (0 means 0% opaque / 1 means 100% opaque) - Note : Do not input below 0.25 or else you will not see it :D" class="form-control" value="{{Auth::user()->opacity}}" name="opa"/>
                </div>
                <div class="form-group col-lg-3 col-md-3 col-sm-3">
                    <label><input type="radio" name="pref" value="1"/>Background Image</label>
                    <input type='file' class="form-control" id="file" name="file"/>
                </div>
                @elseif(Auth::user()->backimage =='1')
                    <label> <input type="radio" name="pref" value="0" />Background Color</label>
                    <input type='color' class="form-control" value="{{Auth::user()->bc}}" name="bc"/>
            </div>

            <div class="form-group col-lg-2 col-md-2 col-sm-2">
                <label>Transparency</label>
                <input type='text' data-toggle="tool-tip" title="0 - 1 (0 means 0% opaque / 1 means 100% opaque) - Note : Do not input below 0.25 or else you will not see it :D" class="form-control" value="{{Auth::user()->opacity}}" name="opa"/>
            </div>
            <div class="form-group col-lg-4 col-md-4 col-sm-4">
                <label><input type="radio" name="pref" value="1" checked/>Background Image</label>
                <input type='file' class="form-control" id="file" name="file"/>
            </div>

        @endif

    <center>
        <button style="margin-bottom: 50px;" class="btn btn-{{Auth::user()->buttons}} btn-lg" type="submit"> Save
            Changes
        </button>
    </center>
    </div>
    </div>

    </form>
    <hr>
</div>
</div>
