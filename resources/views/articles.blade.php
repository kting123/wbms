<style>
    #table {
        border: 0;
    }

    #table tr {
        display: flex;
    }

    #table td {
        flex: 1 auto;
        width: 2px;
    }

    #table thead tr:after {
    }

    #table thead th {
        flex: 1;
    }

    #table tbody {
        display: block;
        width: 100%;
        overflow-y: auto;
        height: 400px;
    }

</style>
<div class="col-lg-10">
    <div class="panel panel-{{Auth::user()->panels}}">
        <div class="panel-heading"><h4>Manage Articles</h4></div>
        <div class="table table-responsive">
            <table class="table table-hover" id="table">
                <thead>
                <th>ID</th>
                <th>Name</th>
                <th>Action</th>
                </thead>
                <tboby>
                    @foreach($articles as $article)
                        <tr>
                            <td>{{$article['id']}}</td>
                            <td>{{$article['name']}}</td>
                            @if($article['id']=='1')
                                <td><label for="" class="label label-primary">Reserved</label></td>
                            @else
                                <td><a data-toggle="modal" href="#" data-target="#edit{{$article['id']}}">  &nbsp;&nbsp;&nbsp;<span
                                                class="glyphicon glyphicon-edit"></span></a> &nbsp; | &nbsp;<a href="#"
                                                                                                               data-toggle="modal"
                                                                                                               data-target="#delete{{$article['id']}}"> <span
                                                class="glyphicon glyphicon-trash"></span></a>

                                    @endif
                                </td>
                        </tr>
                    @endforeach
                </tboby>

            </table>
            <button data-toggle="modal" data-target="#addarticle" class="btn btn-{{Auth::user()->buttons}} btn-lg pull-right"
                    style="margin-top: 5px;margin-right: 5px;"> Add New Article
            </button>
        </div>
        <br>


    </div>
</div>

<div class="modal fade" id="addarticle" role="dialog">
    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">&times;</button>
                <h2 class="modal-title">Add Article</h2>
            </div>
            <div class="modal-body">
                <form type="hidden" method="post"
                      action="./add_article" id="form1"/>
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                <div class="container col-lg-12  col-md-12">
                    <div class="form-group col-lg-12 col-md-12 col-sm-12">
                        <label for="DR">Article Name</label>
                        <input type="text" name="article" value="" placeholder="name here"
                               class="form-control">
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-danger"><span
                            class="glyphicon glyphicon-remove"></span> Cancel
                </button>
                <button type="submit" class="btn btn-success"><span
                            class="glyphicon glyphicon-ok"></span>
                    Save
                </button>
            </div>
        </div>
        </form>
    </div>

</div>
@foreach($articles as $article)
    <div class="modal fade" id="delete{{$article['id']}}" role="dialog">
        <div class="modal-dialog modal-sm">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close"
                            data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Remove Article</h4>
                </div>
                <div class="modal-body">
                    <form type="hidden" method="post"
                          action="./delete_article{{$article['id']}}" id="form1"/>
                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                    <div class="container col-lg-12  col-md-12">
                        <h5> Are you sure you want to delete this Article?</h5>
                    </div>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-danger"><span
                                class="glyphicon glyphicon-remove"></span> Cancel
                    </button>
                    <button type="submit" class="btn btn-success"><span
                                class="glyphicon glyphicon-ok"></span>
                        Ok
                    </button>
                </div>
            </div>
            </form>
        </div>

    </div>


    <div class="modal fade" id="edit{{$article['id']}}" role="dialog">
        <div class="modal-dialog modal-sm">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close"
                            data-dismiss="modal">&times;</button>
                    <h2 class="modal-title">Edit Article</h2>
                </div>
                <div class="modal-body">
                    <form type="hidden" method="post"
                          action="./edit_article{{$article['id']}}" id="form1"/>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                    <div class="container col-lg-12  col-md-12">
                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <label for="DR">Article Name</label>
                            <input type="text" name="article"
                                   value="{{$article['name']}}"
                                   placeholder="name here"
                                   class="form-control">
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-danger"><span
                                class="glyphicon glyphicon-remove"></span> Cancel
                    </button>
                    <button type="submit" class="btn btn-success"><span
                                class="glyphicon glyphicon-ok"></span>
                        Save
                    </button>
                </div>
            </div>
            </form>
        </div>

    </div>
@endforeach
