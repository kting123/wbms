@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">You are logged in as <strong>User</strong> of this system.</div>
                    <div class="panel-body">
                        <center>
                            </br>
                            </br>
                            <div class="form-group col-lg-12">
                                <a href = "./baka">
                                    <div class="shadow col-lg-4">
                                        </br></br></br></br>
                                        <label style = "font-size: medium;"><strong>&nbsp;Add A Resource</strong></label>
                                    </div>

                                </a>

                                <a href = "./transactions">
                                    <div class="shadow col-lg-4">
                                        </br></br></br></br>
                                        <label style = "font-size: medium; "><strong>&nbsp;Out Transactions</strong></label>
                                    </div>
                                </a>
                                <a href = "./vendor">
                                    <div class="shadow col-lg-4">
                                        </br></br></br></br>
                                        <label style = "font-size: medium; "><strong>&nbsp;Vendor to Vendor Transactions</strong></label>
                                    </div>
                                </a>
                            </div>

                        </center>
                        </br>
                        </br>
                        </br>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>

    </script>

    <style>
        div.shadow {
            width: 200px;
            height: 200px;
            margin: 20px;
            border-radius: 20%;
            border-style: solid;
            border-width: 2px;
            border-color: 	#808080;
            padding: 10px;
            background: 	#F0FFFF;
        }

        div.shadow:hover {

            box-shadow: 0 0 20px #A9A9A9;
        }
    </style>
@endsection
