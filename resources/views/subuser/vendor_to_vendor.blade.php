@extends('app')

@section('content')
    <style>
        #arrow1 {
            font-weight: bold;
        }
        #arrow {
            visibility: hidden;
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="panel panel-info">
                <div class="panel-heading" style="height:50px;">
                    <h4 class="pull-left" style="margin-top: 5px">Purchased From Suppliers</h4>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-hover col-lg-12">
                            <thead>
                            <th>Date Purchased</th>
                            <th>Supplier</th>
                            <th>DR #</th>
                            <th>Total Amount (Php)</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                            @foreach($vendor as $vendor)
                            <tr>

                                <td>{{ $vendor->date}}</td>
                                <td>{{ $vendor->ordered_from}}</td>
                                <td>{{ $vendor->dr}}</td>
                                <td>{{ $vendor->amount}}</td>
                                <?php $i=0 ; ?>
                                @foreach($vendor['article_resources'] as $query)
                                    <?php
                                        $i = $i + $query->remain_qty;
                                    ?>
                                @endforeach

                                @if($i > 0)
                                    <td><a href="./vendor_articleOrder{{$vendor->id}}"><span class="glyphicon glyphicon-shopping-cart"></span></a></td>

                                @else
                                    <td><span class="glyphicon glyphicon-minus-sign"></span></td>
                                     @endif
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="pull-right">
                        <a>
                            <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">New
                                Purchase
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content" style="background-image: url('assets/img/123.jpg')">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2 class="modal-title">New Purchase</h2>
                </div>
                <form type="hidden" method="post" action="./addPurchase" id="form1">
                    <div class="modal-body">
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                        <div class="container col-lg-12">
                            <div class="form-group col-lg-3  col-md-3 col-sm-4">
                                <label for="Date">Date Purchased</label>

                                <input type='text' name="datePurchased" class="form-control"
                                       id='datetimepicker0'/>

                            </div>
                            <div class="form-group col-lg-3 col-md-3 col-sm-4">
                                <label for="OR">DR#</label>
                                <input type="text" name="dr#" value=""
                                       placeholder="" class="form-control" id="dr#">
                            </div>
                            <div class="form-group col-lg-3 col-md-3 col-sm-4">
                                <label for="Sold">Supplier</label>
                                <input type="text" name="supplier" value="" placeholder=""
                                       class="form-control" id="supplier">
                            </div>
                            <div class="form-group col-lg-3  col-md-3 col-sm-4">
                                <label for="Date">Due Date</label>

                                <input type='text' name="dueDate" class="form-control"
                                       id='datetimepicker3'/>

                            </div>
                            <div class="form-group col-lg-3 col-md-3 col-sm-4">
                                <label for="articles1">Articles</label>
                                <select id="articles1" class="form-control" size="1">
                                    @foreach($article as $art)
                                        <option>{{ $art->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-lg-2 col-md-3 col-sm-4">
                                <label for="Qty">Qty</label>
                                <input type="number" value="" placeholder="kg" class="form-control"
                                       id="Qty1">
                            </div>
                            <div class="form-group col-lg-2 col-md-3 col-sm-4">
                                <label for="unit1">Unit Price</label>
                                <input type="number" value="" placeholder="" class="form-control"
                                       id="unit1">

                            </div>
                            <div class=" col-lg-1">
                                <label for="Add">Add</label>
                                <button class="btn btn-primary" type="button"
                                        onclick="AddData1()"><span
                                            class="glyphicon glyphicon-plus"></span>
                                </button>
                            </div>
                            <div class="container col-lg-12">
                                <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                    <div class="table table-responsive">
                                        <table class="table table-condensed" id="list">
                                            <thead id="tblHead">
                                            <tr>
                                                <th>Articles</th>
                                                <th>Qty. (Kg)</th>
                                                <th>Unit Price</th>
                                                <th>Total Price (Php)</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                    <label for="Total"><h3>Total Amount (Php):</h3></label>
                                    <label for="TAmount"><h3><input id="totalD1"  value="0.00"
                                                                    name="Tamnt"
                                                                    style='width: 100px;color: #21305c; background-color: transparent;height: 100%;width: 100%; border: 0;padding: 0px 0px 0px;'/>
                                        </h3></label>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button data-dismiss="modal" class="btn btn-danger"><span
                                        class="glyphicon glyphicon-remove"></span> Cancel
                            </button>
                            <button type="submit" class="btn btn-success"><span
                                        class="glyphicon glyphicon-save"></span> Confirm
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        document.getElementById("arrow1").innerHTML = "  Vendors";
        $(document).ready(function () {
            $(function () {
                $('#datetimepicker0').datepicker();
                $('#datetimepicker3').datepicker();

            });
        });

        var totalD = 0;
        var i = 0;

        function AddData1() {
            var rows = "";
            var totalP = "";
            var articles = document.getElementById("articles1").value;
            var qty = document.getElementById("Qty1").value;
            var unit = document.getElementById("unit1").value;

            //var totalD = document.getElementById("totalD").innerHTML;
            var con = parseInt(totalD);

            totalP = qty * unit;
            var TotalP = totalP.toFixed(2);
            totalD = totalD + totalP;
            var TotalD = totalD.toFixed(2);
            rows += "<tr><td><input name=articleV" + i + " value= '" + articles + "' style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;' ></td><td><input name=qtyV" + i + " value= '" + qty + "' style='width: 30px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;' ></td><td><input name=unitPV" + i + " value= '" + unit + "' style='width: 30px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;' ></td><td><input name=TotalPV" + i + " value= '" + TotalP + "' style='width: 80px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;' ></td></tr>";
            $(rows).appendTo("#list tbody");
              //alert(totalP);
            document.getElementById("totalD1").value = TotalD;
            i++;
        }

    </script>
@endsection
