@extends('app')

@section('content')
{{--<style>--}}
{{--.modal-header {--}}
{{--padding: 2px;--}}
{{--}--}}
{{--</style>--}}

<style>
    .hover {
        box-shadow: 10px 10px 5px grey;
    }

    div.shadow {
        width: 300px;
        margin: 20px;
        border-radius: 20%;
        padding: 10px;
        background: #F0F8FF;
    }

    div.shadow:hover {
        box-shadow: 0 0 20px #00FFFF;
    }

    #arrow1 {
        font-weight: bold;
    }

    td, th {
        padding: 2px;
    }

    #arrow {
        visibility: hidden;
    }
</style>

<div class="container">
    <div class="row">
        {{-- start generating the data for farm --}}
        <?php $cowCounter = 1; ?>
        @foreach($cow as $cows)
        @foreach($cows['article_resources'] as $item)

        <input hidden value="{{$cows->id}}" id="cowID{{$cowCounter}}"/>
        <input hidden value="{{$item['article']['name']}}"
               id="cowArt{{$cowCounter.'num'.$cows->id}}">
        <input id="cowArtQ{{$cowCounter.'num'.$cows->id}}" hidden
               value="{{$item['remain_qty']}}">

        <?php $cowCounter++; ?>
        @endforeach
        @endforeach
        <input type="hidden" id="loopNumCow" value="{{$cowCounter-1}}">
        {{-- end here --}}

        {{-- start generating the data for tabo --}}
        <?php $cowCounter1 = 1; ?>
        @foreach($tabo_tabos as $tabo)
        @foreach($tabo['article_resources'] as $item)
        <input hidden value="{{$tabo->id}}" id="taboID{{$cowCounter1}}"/>
        <input hidden value="{{$item['article']['name']}}"
               id="taboArt{{$cowCounter1.'num'.$tabo->id}}">
        <input id="taboArtQ{{$cowCounter1.'num'.$tabo->id}}" hidden
               value="{{$item['remain_qty']}}">
               <?php $cowCounter1++; ?>
        @endforeach
        @endforeach
        <input type="hidden" id="loopNumTabo" value="{{$cowCounter1-1}}">
        {{--end here--}}

        {{-- start generating the data for vendor --}}
        <?php $cowCounter2 = 1; ?>
        @foreach($outsources as $outsource)
        @foreach($outsource['article_resources'] as $item)
        <input hidden value="{{$outsource->id}}" id="vendorID{{$cowCounter2}}"/>
        <input hidden value="{{$item['article']['name']}}"
               id="vendorArt{{$cowCounter2.'num'.$outsource->id}}">
        <input id="vendorArtQ{{$cowCounter2.'num'.$outsource->id}}" hidden
               value="{{$item['remain_qty']}}">
               <?php $cowCounter2++; ?>
        @endforeach
        @endforeach
        <input type="hidden" id="loopNumVendor" value="{{$cowCounter2-1}}">
        {{--End here--}}
        <div class="container col-lg-10 col-lg-offset-1">
            <div class="panel panel-{{Auth::user()->panels}} ">
                <form type="hidden" method="post" action="./addOrder" id="form1">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                    <div class="panel-heading" style="height:50px; margin-top: 5px;">
                        <h4>Order Lists - Source : <select id="choose">
                                <option value="farm">From Farm</option>
                                <option value="tabo">From Tabo</option>
                                <option value="vendor">From Vendor</option>
                            </select></h4>
                    </div>
                    <div class="panel-body">
                        <div class="container col-lg-12">
                            <div class="form-group col-lg-3">
                                <label class="radio-inline">
                                    <input type="radio" name="radBtn" id="Delivery" value="1" checked> <b>
                                        Delivery</b>
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="radBtn" id="WalkeIn" value="0"> <b>Walk-In </b>
                                </label>
                                &nbsp;&nbsp;
                            </div>
                            <label class="checkbox-inline" style="margin-left: 10px;"> <input
                                    type="checkbox"
                                    id="getFrom"> Get
                                from
                                Available Articles&copy;</input>
                            </label>
                        </div>
                        <div class="container col-lg-12 col-md-12 col-sm-12 ">
                            @include('subuser.delivery')
                            @include('subuser.walk_in')

                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <label for="dueDate">Ref_ID</label>
                                <select id="changeCow"
                                        class="form-control"
                                        required>
                                    <option value="">Select</option>
                                    <optgroup id="farm1" label="From Farm">
                                        <?php $i = 0; ?>
                                        @foreach($cow as $cows)
                                        @foreach($cows['article_resources'] as $item)
                                        <?php $i = $i + $item->remain_qty; ?>
                                        @endforeach
                                        @if($i=='0')
                                        @else
                                        <option value="{{$cows->id}}">{{$cows->id}}</option>
                                        @endif
                                        @endforeach
                                    </optgroup>
                                    <optgroup id="tabo1" hidden label="From Tabo">
                                        <?php $k = 0; ?>
                                        @foreach($tabo_tabos as $tabo)
                                        @foreach($tabo['article_resources'] as $item1)
                                        <?php $k = $k + $item1->remain_qty; ?>
                                        @endforeach
                                        @if($k=='0')
                                        @else
                                        <option value="{{$tabo->id}}">{{$tabo->id}}</option>
                                        @endif
                                        @endforeach
                                    </optgroup>
                                    <optgroup id="vendor1" hidden label="From Vendor">
                                        <?php $j = 0; ?>
                                        @foreach($outsources as $outsource)
                                            @foreach($outsource['article_resources'] as $item2)
                                            <?php $j = $j + $item2->remain_qty; ?>
                                            @endforeach
                                            @if($j==0)
                                            @endif
                                            @if($j>0)
                                            <option value="{{$outsource->id}}">{{$outsource->id}}</option>
                                            @endif
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                <label for="gender">Articles</label>
                                <select id="articles" class="form-control" size="1">
                                    @foreach($articles as $article)
                                    <option value="{{$article->name}}">{{$article->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-lg-2 col-md-2 col-sm-2">
                                <label for="Qty">Qty</label>
                                <input type="text" value="" placeholder="kg" class="form-control"
                                       id="Qty">
                            </div>

                            <div class="form-group col-lg-2 col-md-2 col-sm-2">
                                <label for="unit">Unit Price</label>
                                <input type="text" value="" placeholder="" class="form-control"
                                       id="unit">
                            </div>
                            <div class="container col-lg-3 col-md-3 col-sm-4" id="sd" hidden>
                                <label for="articleF">Articles From</label>
                                <select class="form-control" id="articlesFrom">
                                    <option value="null">Select</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-2 col-md-2 col-sm-2" style="margin-top: 22px;">
                                <button class="btn btn-{{Auth::user()->buttons}}" type="button"
                                        onclick="AddData()">
                                    <span class="glyphicon glyphicon-plus"></span>Add Item
                                </button>
                                <!-- <input id = "button" type = "button" value = " Add " onclick = "AddData()"> -->
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="table table-responsive">
                                    <table class="table table-condensed" id="list1">
                                        <thead id="tblHead">
                                            <tr>
                                                <th>Articles</th>
                                                <th>Qty. (Kg)</th>
                                                <th>Unit Price</th>
                                                <th>Total Price (Php)</th>
                                                <th>Articles From</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <label for="Total"><h3>Total Due (Php):</h3></label>
                                <label for="TAmount"><h3><input id="totalD" value="0.00" name="Tamount"
                                                                style='width: 100px;color: #000000; background-color: transparent;height: 100%;width: 100%; border: 0;padding: 0px 0px 0px;'/>
                                    </h3></label>
                            </div>
                        </div>
                        <div class="form-group pull-right">
                            <a href="./transactions" class="btn btn-danger"><span
                                    class="glyphicon glyphicon-refresh"></span> Refresh All
                            </a>
                            <a onclick="ResetTable()" class="btn btn-warning"><span
                                    class="glyphicon glyphicon-trash"></span> Reset List</a>
                            <button type="submit" class="btn btn-{{Auth::user()->buttons}}"><span
                                    class="glyphicon glyphicon-shopping-cart"></span> Checkout
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    document.getElementById("arrow1").innerHTML = "  Orders";
    $(document).ready(function () {
        $('input[type="radio"]').click(function () {
            if ($(this).attr('id') == 'Delivery') {
                $('#deliveryShow').show();
                $('#walk_inShow').hide();
                $('#Sold').val("kim");
                $('#OR').val("143");
            }
            else {
                $('#deliveryShow').hide();
                $('#walk_inShow').show();
                $('#Deliver').val("kim");
                $('#DR').val("143");
            }
        });
    });
    $(function () {
        $('#datetimepicker6').datepicker("setDate", '1d' + 30);
        $('#datetimepicker4').datepicker("setDate", '1d');
        $('#datetimepicker5').datepicker("setDate", '1d');

    });
    var totalD = 0;
    var i = 0;
    var check = 0;
    var articlesFrom;
    var qtyFrom;
    var articles;
    var qty;
    function AddData() {
        var rows = "";
        var totalP = "";
        var articles = document.getElementById("articles").value;
        var qty = document.getElementById("Qty").value;
        var unit = document.getElementById("unit").value;
        var id = document.getElementById("changeCow").value;
        var type = document.getElementById("choose").value;
        if (id == '') {
            alert("Please input valid Ref_ID.");
        }
        else if (qty == '') {
            alert("Please input valid quantiy.");
        }
        else if (unit == '') {
            alert("Please input valid unit price.");
        }
        else {
            if (check === 1) {
                articlesFrom = document.getElementById("articlesFrom").value;
            }
            else if (check === 0) {
                articlesFrom = articles;
            }
            var con = parseInt(totalD);

            totalP = qty * unit;
            var TotalP = totalP.toFixed(2);
            totalD = totalD + totalP;
            var TotalD = totalD.toFixed(2);
            rows += "<tr><td><input hidden name=id" + i + " value='" + id + "'> <input hidden name=type" + i + " value='" + type + "'> <input name=articleD" + i + " value='" + articles + "' style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;'></td><td><input name=qtyD" + i + " value='" + qty + "' style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;'></td><td><input name=unitPD" + i + " value='" + unit + "' style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;'></td><td><input name=TotalPD" + i + " value='" + TotalP + "' style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;'></td><td><input name=articlesFrD" + i + " value='" + articlesFrom + "' style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;'></td></tr>";
            $(rows).appendTo("#list1 tbody");
            //   alert(totalD);
            document.getElementById("totalD").value = TotalD;
            i++;

        }
    }
    $(document).ready(function () {

        $('#getFrom').click(function () {
            if ($(this).is(":checked")) {
                check = 1;
                $('#sd').show();

            }
            else {
                check = 0;
                $('#sd').hide();
            }
        });
        $("#choose").change(function () {
            if ($(this).val() == 'farm') {
                $('#farm1').show();
                $('#tabo1').hide();
                $('#vendor1').hide();
            }
            if ($(this).val() == 'tabo') {
                $('#tabo1').show();
                $('#farm1').hide();
                $('#vendor1').hide();
            }
            if ($(this).val() == 'vendor') {
                $('#tabo1').hide();
                $('#farm1').hide();
                $('#vendor1').show();
            }

        });
        $("#changeCow").change(function () {
            var selected1 = $(this).val();
            if ($('#choose').val() == 'farm') {
                var loopNumCow = document.getElementById('loopNumCow').value;
                $("#articlesFrom").find('option').remove().end().append('<option value="Select">Select</option>')
                if (selected1 != 'null') {
                    for (var i = loopNumCow; i >= 0; i--) {
                        if (document.getElementById('cowID' + i).value == selected1) {
                            var num1 = document.getElementById('cowID' + i).value;
                            var Artname = document.getElementById('cowArt' + i + 'num' + num1).value;
                            var ArtQ = document.getElementById('cowArtQ' + i + 'num' + num1).value;
                            if (ArtQ == 0) {
                            }
                            else {
                                $('#articlesFrom').append('<option value ="' + Artname + '">' + Artname + '-' + ArtQ + '</option>');
                            }
                        }
                    }
                }
            }
            else if ($('#choose').val() == 'tabo') {
                var loopNumTabo = document.getElementById('loopNumTabo').value;
                $("#articlesFrom").find('option').remove().end().append('<option value="Select">Select</option>')
                if (selected1 != 'null') {
                    for (var i = loopNumTabo; i >= 0; i--) {
                        if (document.getElementById('taboID' + i).value == selected1) {
                            var num1 = document.getElementById('taboID' + i).value;
                            var Artname = document.getElementById('taboArt' + i + 'num' + num1).value;
                            var ArtQ = document.getElementById('taboArtQ' + i + 'num' + num1).value;
                            if (ArtQ == 0) {
                            }
                            else {
                                $('#articlesFrom').append('<option value ="' + Artname + '">' + Artname + '-' + ArtQ + '</option>');
                            }
                        }
                    }
                }
            }
            else if ($('#choose').val() == 'vendor') {
                var loopNumVendor = document.getElementById('loopNumVendor').value;
                $("#articlesFrom").find('option').remove().end().append('<option value="Select">Select</option>')
                if (selected1 != 'null') {
                    for (var i = loopNumVendor; i >= 0; i--) {
                        if (document.getElementById('vendorID' + i).value == selected1) {
                            var num1 = document.getElementById('vendorID' + i).value;
                            var Artname = document.getElementById('vendorArt' + i + 'num' + num1).value;
                            var ArtQ = document.getElementById('vendorArtQ' + i + 'num' + num1).value;
                            if (ArtQ == 0) {
                            }
                            else {
                                $('#articlesFrom').append('<option value ="' + Artname + '">' + Artname + '-' + ArtQ + '</option>');
                            }
                        }
                    }
                }
            }
        });
    });
    function ResetTable() {
        i = 0;
        $('#list1 tbody tr').remove();
    }
</script>
@endsection