@extends('app')

@section('content')
<style>
    #arrow1 {
        font-weight: bold;
    }
    #arrow {
        visibility: hidden;
    }
</style>
<div class="container">
    <div class="row">
        <div class="panel panel-info col-lg-3">
            <div class="panel-heading" style="height:50px; margin-top: 5px;">
                Supplier Transaction Breakdown
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-hover col-lg-12">
                        <thead>
                        <th>Article</th>
                        <th>Quantity Left (kg)</th>
                        </thead>
                        <tbody>
                            @foreach($articles as $article)
                            <tr>
                                <td>{{$article['article']['name']}}</td>
                                @if($article->remain_qty == 0)
                                <td><label class="label label-default">{{$article->remain_qty}}</label></td>
                                @elseif($article->remain_qty > 0)
                                <td><label class="label label-success">{{$article->remain_qty}}</label></td>
                                @elseif($article->remain_qty < 0)
                                <td><label class="label label-default">{{$article->remain_qty}}</label></td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
        <div class="panel panel-info col-lg-8 col-lg-offset-1">
            <div class="panel-heading" style="height:50px; margin-top: 5px;">
                Order List
            </div>
            <div class="panel-body">
                <form type="hidden" method="post" action="./add_outsourceOrder" id="form1">
                    <input type="hidden" name="id" value="{{$articles['0']['outsource_id']}}"/>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                    <div class="container col-lg-12">
                        <div class="form-group col-lg-3">
                            <label for="Date">Date</label>

                            <input type='text' name="date1" class="form-control"
                                   id='datetimepicker4' id="date"/>

                        </div>
                        <div class="form-group col-lg-2">
                            <label for="DR">DR#</label>
                            <input type="text" name="dr" placeholder="" class="form-control" id="DR#">
                        </div>
                        <div class="form-group col-lg-4">
                            <label for="Deliver">Delivered To</label>
                            <input type="text" name="deliver" value="" placeholder=""
                                   class="form-control" id="Deliver">
                        </div>

                        <div class="container col-lg-3">
                            <label for="dueDate">Due Date</label>
                            <input type='text' class="form-control" name="date2"
                                   id='datetimepicker5' id="date1"/>
                        </div>
                        <div class="container col-lg-12">
                            <div class="form-group col-lg-5">
                                <label for="gender">Articles</label>
                                <select id="articles" class="form-control" size="1">
                                    @foreach($articles as $article)
                                    <option>{{ $article['article']['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-lg-2">
                                <label for="Qty">Qty</label>
                                <input type="text" value="" placeholder="kg" class="form-control"
                                       id="Qty">
                            </div>

                            <div class="form-group col-lg-2">
                                <label for="unit">Unit Price</label>
                                <input type="text" value="" placeholder="" class="form-control"
                                       id="unit">
                            </div>
                            <div class="form-group col-lg-1">
                                <label for="Add">Add</label>

                                <button class="btn btn-primary" type="button" onclick="AddData()">
                                    <span class="glyphicon glyphicon-plus"></span></button>
                                <!-- <input id = "button" type = "button" value = " Add " onclick = "AddData()"> -->
                            </div>
                        </div>
                        <div class="container col-lg-12">
                            <div class="table table-responsive">
                                <table class="table table-condensed" id="list1">
                                    <thead id="tblHead">
                                        <tr>
                                            <th>Articles</th>
                                            <th>Qty. (Kg)</th>
                                            <th>Unit Price</th>
                                            <th>Total Price (Php)</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                            <label for="Total"><h3>Total Due (Php):</h3></label>
                            <label for="TAmount"><h3><input id="totalD" value="0.00" name="Tamount"
                                                            style='width: 100px;color: #000000; background-color: transparent;height: 100%;width: 100%; border: 0;padding: 0px 0px 0px;'/>
                                </h3></label>
                            <div class="form-group pull-right">
                            <a href="./vendor_articleOrder{{$article->outsource_id}}" class="btn btn-danger"><span
                                    class="glyphicon glyphicon-refresh"></span> Refresh All
                            </a>
                            <a onclick="ResetTable()" class="btn btn-warning"><span
                                    class="glyphicon glyphicon-trash"></span> Reset List</a>
                            <button type="submit" class="btn btn-success"><span
                                    class="glyphicon glyphicon-shopping-cart"></span> Make a Delivery
                            </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    document.getElementById("arrow1").innerHTML = "  Forward";
    $(document).ready(function () {
        $(function () {
            $('#datetimepicker4').datepicker("setDate", '1d');
            $('#datetimepicker5').datepicker("setDate", '1d');

        });
    });

    var totalD = 0;
    var i = 0;

    function AddData() {
        var rows = "";
        var totalP = "";
        var articles = document.getElementById("articles").value;
        var qty = document.getElementById("Qty").value;
        var unit = document.getElementById("unit").value;
        var articlesFrom;
        var qtyFrom;

        //var totalD = document.getElementById("totalD").innerHTML;
        var con = parseInt(totalD);

        totalP = qty * unit;
        var TotalP = totalP.toFixed(2);
        totalD = totalD + totalP;
        var TotalD = totalD.toFixed(2);
        rows += "<tr><td><input name=articleD" + i + " value='" + articles + "' style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;'></td><td><input name=qtyD" + i + " value='" + qty + "' style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;'></td><td><input name=unitPD" + i + " value='" + unit + "' style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;'></td><td><input name=TotalPD" + i + " value='" + TotalP + "' style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;'></td></tr>";
        $(rows).appendTo("#list1 tbody");
        //   alert(totalD);
        document.getElementById("totalD").value = TotalD;
        i++;
    }

    function ResetTable() {
        i = 0;
        $('#list1 tbody tr').remove();
    }
</script>
@endsection
