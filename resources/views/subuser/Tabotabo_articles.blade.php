@extends('app')

@section('content')
<style>
    #arrow0 {
        font-weight: bold;
    }

    .table td {
        text-align: left;
        padding: 2px;
    }

    .panel-body {
        padding: 5px;
    }
    .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
        padding: 2px;
    }
    
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-lg-8 col-sm-8 col-lg-offset-2">
            <div class="panel panel-{{Auth::user()->panels}}">
                <div class="panel-body" style="background-image: url('assets/img/.jpg')">
                    <div vlass = "well" style="text-align: center"><span class="glyphicon glyphicon-minus-sign"></span> Date of Purchase:
                        <b>{{$tabo_tabo['date_of_purchase']}}</b> &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp; <span
                            class="glyphicon glyphicon-minus-sign"></span> Body# : <b>{{$tabo_tabo['id']}}</b> &nbsp;&nbsp;
                        &nbsp; &nbsp;&nbsp; &nbsp;<span class="glyphicon glyphicon-minus-sign"></span> LW :
                        <b>{{$tabo_tabo['lw']}}</b> Kls. &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; <span
                            class="glyphicon glyphicon-minus-sign"></span> SW : <b>{{$tabo_tabo['sw']}}</b> Kls.
                        &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp; <span class="glyphicon glyphicon-minus-sign"></span>
                        MW : <b>{{$tabo_tabo['mw']}}</b> Kls.&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp; <span
                            class="glyphicon glyphicon-minus-sign"></span> <b>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8 col-lg-offset-2" >
            <div>
                <div class="panel panel-{{Auth::user()->panels}}">
                    <div class="panel-heading" style="height: 80px;">
                        <h3 class="pull-left" style="margin-top: 20px"> Tabo - Tabo </h3>
                        <label class="pull-right">{!! $resource->render() !!}</label>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-hover col-lg-12 col-md-12 col-xs-12">
                                <thead style="border-bottom:solid 2px;">
                                <th></th>
                                <th>Article Name</th>
                                <th>Quantity (Kg)</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                    
                                    @foreach($resource as $tabo_resource)
                                    <tr>                               
                                        <td></td>
                                        <td>{{ $tabo_resource['article']['name'] }}</td>
                                        <td>{{ $tabo_resource['quantity']}}</td>
                                        <td>
                                            <a data-toggle="modal" data-target="#editTaboArticle{{$tabo_resource['article_id']}}"><span class="glyphicon glyphicon-edit"></span></a>|
                                            <a data-toggle="modal" data-target="#deleteTaboArticle{{$tabo_resource['article_id']}}"><span class="glyphicon glyphicon-trash"></span></a>    
                                        </td>                                
                                    </tr>   
                                    @endforeach                                    
                                <tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        @foreach($resource as $tabo_resource)
        <div class="modal fade" id="deleteTaboArticle{{$tabo_resource['article_id']}}" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close"
                                data-dismiss="modal">&times;</button>
                        <h2 class="modal-title">Delete {{$tabo_resource['article']['name']}}</h2>
                    </div>
                    <div class="modal-body">
                        <form type="hidden" method="post"
                              action="./deleteTaboArticle{{$tabo_resource['article_id']}}" id="form1"/>
                        <input type="hidden" name="_token"
                               value="{{{ csrf_token() }}}"/>
                        <input type="hidden" name="tabo_tabo_id"
                               value="{{$tabo_resource['tabo_tabo_id']}}"/>
                        <input type="hidden" name="article_id"
                               value="{{$tabo_resource['article_id']}}"/>
                        <div class="container col-lg-12  col-md-12">
                            <h5> Are you sure you want to delete this article?</h5>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-danger"><span
                                class="glyphicon glyphicon-remove"></span> Cancel
                        </button>
                        <button type="submit" class="btn btn-{{Auth::user()->buttons}}"><span
                                class="glyphicon glyphicon-ok"></span>
                            Confirm
                        </button>
                    </div>
                </div>
                </form>
            </div>
        </div>

        <div class="modal fade center" id="editTaboArticle{{$tabo_resource['article_id']}}" role="dialog">
            <div class="modal-dialog ">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close"
                                data-dismiss="modal">&times;</button>
                        <h2 class="modal-title">Edit {{ $tabo_resource['article']['name']}}</h2>
                    </div>
                    <form method="post" action="./editTaboArticle{{ $tabo_resource['article_id']}}">
                        <input type="hidden" name="_token"
                               value="{{csrf_token() }}"/>
                        <input type="hidden" name="tabo_tabo_id" id="tabo_tabo_id"
                               value="{{ $tabo_resource['tabo_tabo_id'] }}"/>                        
                        <input type="hidden" name="old_qty" id="old_qty"
                               value="{{ $tabo_resource['quantity'] }}"/>
                        <div class="modal-body">
                            <div id="parentDiv"
                                 class="container col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group col-lg-7 col-md-7 col-sm-7">
                                    <label for="part">Parts</label>
                                    <select name="article" class="form-control" size = "1">    
                                        <option>{{ $tabo_resource['article']['name'] }}</option>
                                        @foreach($articles as $art)
                                        <option >{{ $art->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                    <label for="Qty">Qty (kg):</label>
                                    <input type="text" name="qty" value="{{ $tabo_resource['quantity']}}"
                                           placeholder="in kg" class="form-control"
                                           id="Qty">
                                </div>
                            </div>                           
                        </div>
                        <div class="modal-footer">
                            <button data-dismiss="modal"
                                    class="btn btn-danger"><span
                                    class="glyphicon glyphicon-remove"></span>
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-{{Auth::user()->buttons}}"><span
                                    class="glyphicon glyphicon-save"></span>
                                Save
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        @endforeach

       

        <script>
                    document.getElementById("arrow0").innerHTML = "  Add a Resource";
                    document.getElementById("arrow1").innerHTML = "  Body # {{$tabo_tabo['id']}}";
        </script>
        @endsection
